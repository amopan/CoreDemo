﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// ReSharper disable once CheckNamespace
namespace CoreDemo.Persistance
{
    using System.Linq.Expressions;
    using System.Threading;

    using Microsoft.EntityFrameworkCore;
    public static class EfRepositoryExt
    {
        public static Task<TSource> SingleOrDefaultAsync<TSource>(
            this IQueryable<TSource> source,
            Expression<Func<TSource, bool>> predicate,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            return EntityFrameworkQueryableExtensions.SingleOrDefaultAsync(
                source,
                predicate,
                cancellationToken);
        }

        public static Task<List<TSource>> ToListAsync<TSource>(this IQueryable<TSource> source, CancellationToken cancellationToken = default(CancellationToken))
        {
            return EntityFrameworkQueryableExtensions.ToListAsync(source,cancellationToken);
        }
    }
}
