﻿namespace CoreDemo.Persistance
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.Contracts;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using CoreDemo.Common.DataAccess;
    using CoreDemo.Common.Entities;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.ChangeTracking;

    internal class EfRepository<T, TKey> : IRepositoryAsync<T, TKey>
        where T : class, IEntity<TKey>
    {
        private readonly DbSet<T> dbSet;

        public EfRepository( /*IUnitOfWork<T, TKey> unitOfWork*/ DbContext context)
        {
            Contract.Requires(context != null);
            //unitOfWork.Register(this);
            this.Context = context;
            this.dbSet = context.Set<T>();
        }

        protected DbContext Context { get; }

        public void Add(T entity)
        {
            Contract.Requires(entity != null);
            var  entry = this.Context.Entry(entity);
            if (!entity.Id.Equals(default(TKey)) && entry.State == EntityState.Detached)
            {
                DbSet<T> set = this.Context.Set<T>();
                var localEntries = this.Local(entity.Id);
                if (localEntries.Any())
                {
                    entry = localEntries.First();
                }
                else
                {
                    set.Attach(entity);
                }
                entry.State = EntityState.Modified;
            }
            else
            {
                if (entry.State != EntityState.Unchanged)
                {
                    if (Equals(entity.Id, default(TKey)))
                    {
                        this.Context.Entry(entity).State = EntityState.Added;
                        this.Context.Set<T>().Add(entity);
                    }
                    else
                    {
                        this.Context.Set<T>().Attach(entity);
                        this.Context.Entry(entity).State = EntityState.Modified;
                    }
                }

            }
        }

        public void Delete(T entity)
        {
            Contract.Requires(entity != null, "entity can not by a null value");
            var localEntries = this.Local(entity.Id);
            if (localEntries.Any())
            {
                var entry = localEntries.First();
                entry.State = EntityState.Deleted;
            }
            else
            {
                var entry = this.Context.Entry(entity);
                entry.State = EntityState.Deleted;
            }
        }

        public IQueryable<T> Query(
            Expression<Func<T, bool>> predicate = null,
            IEnumerable<Expression<Func<T, object>>> prefetches = null)
        {
            return this.AsQueryable(prefetches).Where(predicate);
        }

        
        public async Task SaveChangesAsync()
        {
            await this.Context.SaveChangesAsync();
        }


        public async Task<T> SingleAsync(TKey id, IEnumerable<Expression<Func<T, object>>> prefetches = null)
        {
            var itemParameter = Expression.Parameter(typeof(T), "item");
            var specification =
                Expression.Lambda<Func<T, bool>>(
                    Expression.Equal(Expression.Property(itemParameter, "Id"), Expression.Constant(id)),
                    itemParameter);

            return await this.SingleAsync(specification, prefetches);
        }

        public async Task<T> SingleAsync(
            Expression<Func<T, bool>> predicate,
            IEnumerable<Expression<Func<T, object>>> prefetches = null)
        {
            Contract.Requires(predicate != null);
            return await this.AsQueryable(prefetches).SingleOrDefaultAsync(predicate);
        }

        public async Task<IEnumerable<T>> ListAsync(Expression<Func<T, bool>> predicate = null, IEnumerable<Expression<Func<T, object>>> prefetches = null)
        {
            var res= await this.Query(predicate, prefetches).ToListAsync();
            return res;
        }

        public void Update(T entity)
        {
            Contract.Requires(entity != null);
            this.Context.Entry(entity).State = EntityState.Modified;
        }

        public IQueryable<T> AsQueryable(IEnumerable<Expression<Func<T, object>>> prefetches = null)
        {
            return prefetches == null
                ? this.dbSet.AsQueryable()
                : prefetches.Aggregate(this.dbSet.AsQueryable(), (current, expression) => current.Include(expression));
        }


        protected IEnumerable<EntityEntry<T>> Local(params object[] keyValues)
        {
            
            var entries = this.Context.ChangeTracker.Entries<T>();

            if (keyValues.Any() == true)
            {
                var entityType = this.Context.Model.FindEntityType(typeof(T));
                var keys = entityType.GetKeys();
                var i = 0;

                foreach (var property in keys.SelectMany(x => x.Properties))
                {
                    var keyValue = keyValues[i];
                    entries = entries.Where(e => keyValue.Equals(e.Property(property.Name).CurrentValue));
                    i++;
                }
            }

            return entries;
        }
    }
}