﻿namespace CoreDemo.Persistance.Configuration
{
    using Autofac;

    using CoreDemo.Common.DataAccess;

    using Microsoft.EntityFrameworkCore;

    public class PersistanceModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<MyDbContext>()
                .As<DbContext>()
                .WithParameter(
                    (pInfo, ctx) => pInfo.Name == "options",
                    (pInfo, ctx) => ctx.Resolve<DbContextOptions<MyDbContext>>())
                .InstancePerLifetimeScope();//.InstancePerRequest();
            builder.RegisterType<UnitOfWorkAsync>().As<IUnitOfWorkAsync>().InstancePerLifetimeScope();//.InstancePerRequest();
            builder.RegisterGeneric(typeof(EfRepository<,>)).As(typeof(IRepositoryAsync<,>)).InstancePerLifetimeScope();
                //.InstancePerRequest();
        }
    }
}