﻿namespace CoreDemo.Persistance
{
    using System;
    using System.Diagnostics;

    using Microsoft.EntityFrameworkCore;
    
    public partial class MyDbContext : DbContext
    {
        private readonly DateTime CreatedOn;
        public MyDbContext(DbContextOptions options)
            : base(options)
        {
            this.CreatedOn = DateTime.Now;
            Debug.WriteLine($"context created {this.CreatedOn.ToString()}");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            this.OnUsersModelCreating(modelBuilder);

            base.OnModelCreating(modelBuilder);
        }

        public override void Dispose()
        {
            base.Dispose();
            Debug.WriteLine($"context disposed {this.CreatedOn.ToString()}");
        }
    }
}