﻿namespace CoreDemo.Persistance
{
    using CoreDemo.Data.Users;

    using Microsoft.EntityFrameworkCore;

    public partial class MyDbContext
    {

        public DbSet<User> Users { get; set; }

        protected virtual void OnUsersModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().ToTable("User");
        }
    }
}