﻿namespace CoreDemo.Persistance
{
    using System;
    using System.Diagnostics;
    using System.Threading.Tasks;

    using CoreDemo.Common.DataAccess;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Storage;

    internal class UnitOfWorkAsync : IUnitOfWorkAsync
    {
        //private readonly IDictionary<string, object> repositories = new ConcurrentDictionary<string, object>();

        public UnitOfWorkAsync(DbContext context)
        {
            this.Context = context;
        }

        public bool InTran
        {
            get
            {
                return this.Transaction != null;
            }
        }

        private IDbContextTransaction Transaction { get; set; }

        protected DbContext Context { get; private set; }

        /*public void Register<T, TKey>(IRepositoryAsync<T, TKey> repository) where T : class, IEntity<TKey>
        {
            Contract.Requires(repository != null);
            this.repositories.Add(repository.GetType().Name, repository);
        }*/

        public async Task<int> ApplyChangesAsync()
        {
            var result = await this.Context.SaveChangesAsync();
            //if (InTran) this.Commit();
            return result;
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        public async Task BeginTransaction()
        {
            if (this.InTran) throw new InvalidOperationException("Other transaction is already started.");
            Debug.WriteLine("try to start a new transaction");
            this.Transaction = await this.Context.Database.BeginTransactionAsync();
        }

        public async Task<int> Commit()
        {
            var res = await this.ApplyChangesAsync();

            if (this.InTran)
            {
                this.Transaction.Commit();
                //this.Transaction.Dispose();
                this.Transaction = null;
            }
            return res;
        }

        public void Rollback()
        {
            if (this.InTran)
            {
                this.Transaction.Rollback();
                //this.Transaction.Dispose();
                this.Transaction = null;
            }
        }

        /*public IRepositoryAsync<T, TKey> Repository<T, TKey>() where T : class, IEntity<TKey>
        {
            var key = typeof(T).Name;
            object repObj;
            this.repositories.TryGetValue(key, out repObj);
            return repObj as IRepositoryAsync<T, TKey>;
        }*/

        private void Dispose(bool disposing)
        {
            if (disposing)
                if (this.Context != null)
                {
                    if (this.InTran) this.Transaction.Commit();
                    this.Context.Dispose();
                    this.Context = null;
                }
        }
    }
}