﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace CoreDemo.IdentityServer
{
    using System.Security.Claims;

    using IdentityServer4;
    using IdentityServer4.Models;
    using IdentityServer4.Services.InMemory;

    using Microsoft.AspNetCore.Http.Features.Authentication;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Options;
    using Microsoft.IdentityModel.Tokens;

    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsDevelopment())
            {
                // For more details on using the user secret store see http://go.microsoft.com/fwlink/?LinkID=532709
                //builder.AddUserSecrets();
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }



        public void ConfigureServices(IServiceCollection services)
        {

            services.AddMvc();

            services.AddIdentityServer()
                 .AddTemporarySigningCredential()
            .AddInMemoryPersistedGrants()
            .AddInMemoryClients(Config.GetClients())
            .AddInMemoryScopes(Config.GetScopes())
            .AddInMemoryUsers(Config.GetUsers());


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            /*loggerFactory.AddConsole(LogLevel.Debug);
            app.UseDeveloperExceptionPage();

            app.UseIdentityServer();

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme,

                AutomaticAuthenticate = false,
                AutomaticChallenge = false
            });

            app.UseGoogleAuthentication(new GoogleOptions
            {
                AuthenticationScheme = "Google",
                DisplayName = "Google",
                SignInScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme,

                ClientId = "434483408261-55tc8n0cs4ff1fe21ea8df2o443v2iuc.apps.googleusercontent.com",
                ClientSecret = "3gcoTrEDPPJ0ukn_aYYT6PWo"
            });

            app.UseStaticFiles();
            app.UseMvcWithDefaultRoute();*/
                        loggerFactory.AddConsole();
                        

                        if (env.IsDevelopment())
                        {
                            app.UseDeveloperExceptionPage();
                            //app.UseDatabaseErrorPage();
                            //app.UseBrowserLink();
                        }
                        else
                        {
                            app.UseExceptionHandler("/Home/Error");
                        }
                        //app.UseMiddleware<KestrelAuthenticationMiddleware>();
                        app.UseIdentityServer();

                        /*// cookie middleware for temporarily storing the outcome of the external authentication
                        app.UseCookieAuthentication(new CookieAuthenticationOptions
                        {
                            AuthenticationScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme,
                            AutomaticAuthenticate = false,
                            AutomaticChallenge = false
                        });

                        // middleware for google authentication
                        app.UseGoogleAuthentication(new GoogleOptions
                        {
                            AuthenticationScheme = "Google",
                            SignInScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme,
                            ClientId = "434483408261-55tc8n0cs4ff1fe21ea8df2o443v2iuc.apps.googleusercontent.com",
                            ClientSecret = "3gcoTrEDPPJ0ukn_aYYT6PWo"
                        });

                        // middleware for external openid connect authentication
                        app.UseOpenIdConnectAuthentication(new OpenIdConnectOptions
                        {
                            SignInScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme,
                            SignOutScheme = IdentityServerConstants.SignoutScheme,

                            DisplayName = "OpenID Connect",
                            Authority = "https://demo.identityserver.io/",
                            ClientId = "implicit",

                            TokenValidationParameters = new TokenValidationParameters
                            {
                                NameClaimType = "name",
                                RoleClaimType = "role"
                            }
                        });
*/



            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseMvcWithDefaultRoute();

            app.UseMvc(routes =>
                        {
                            routes.MapRoute(
                                name: "default",
                                template: "{controller=Home}/{action=Index}/{id?}");
                        });
        }


        public class KestrelAuthenticationMiddleware
        {
            private readonly RequestDelegate _next;
            public KestrelAuthenticationMiddleware(RequestDelegate next)
            {
                _next = next;
            }
            public async Task Invoke(HttpContext context)
            {
                var existingPrincipal = context.Features.Get<IHttpAuthenticationFeature>()?.User;
                var handler = new KestrelAuthHandler(context, existingPrincipal);
                AttachAuthenticationHandler(handler);
                try
                {
                    await _next(context);
                }
                finally
                {
                    DetachAuthenticationhandler(handler);
                }
            }

            private void AttachAuthenticationHandler(KestrelAuthHandler handler)
            {
                var auth = handler.HttpContext.Features.Get<IHttpAuthenticationFeature>();
                if (auth == null)
                {
                    auth = new HttpAuthenticationFeature();
                    handler.HttpContext.Features.Set(auth);
                }
                handler.PriorHandler = auth.Handler;
                auth.Handler = handler;
            }

            private void DetachAuthenticationhandler(KestrelAuthHandler handler)
            {
                var auth = handler.HttpContext.Features.Get<IHttpAuthenticationFeature>();
                if (auth != null)
                {
                    auth.Handler = handler.PriorHandler;
                }
            }
        }

        internal class KestrelAuthHandler : IAuthenticationHandler
        {
            internal KestrelAuthHandler(HttpContext httpContext, ClaimsPrincipal user)
            {
                HttpContext = httpContext;
                User = user;
            }

            internal HttpContext HttpContext { get; }

            internal ClaimsPrincipal User { get; }

            internal IAuthenticationHandler PriorHandler { get; set; }

            public Task AuthenticateAsync(AuthenticateContext context)
            {
                if (User != null)
                {
                    context.Authenticated(User, properties: null, description: null);
                }
                else
                {
                    context.NotAuthenticated();
                }


                if (PriorHandler != null)
                {
                    return PriorHandler.AuthenticateAsync(context);
                }

                return Task.FromResult(0);
            }

            public Task ChallengeAsync(ChallengeContext context)
            {
                bool handled = false;
                switch (context.Behavior)
                {
                    case ChallengeBehavior.Automatic:
                        // If there is a principal already, invoke the forbidden code path
                        if (User == null)
                        {
                            goto case ChallengeBehavior.Unauthorized;
                        }
                        else
                        {
                            goto case ChallengeBehavior.Forbidden;
                        }
                    case ChallengeBehavior.Unauthorized:
                        HttpContext.Response.StatusCode = 401;
                        // We would normally set the www-authenticate header here, but IIS does that for us.
                        break;
                    case ChallengeBehavior.Forbidden:
                        HttpContext.Response.StatusCode = 403;
                        handled = true; // No other handlers need to consider this challenge.
                        break;
                }
                context.Accept();


                if (!handled && PriorHandler != null)
                {
                    return PriorHandler.ChallengeAsync(context);
                }

                return Task.FromResult(0);
            }

            public void GetDescriptions(DescribeSchemesContext context)
            {
                if (PriorHandler != null)
                {
                    PriorHandler.GetDescriptions(context);
                }
            }

            public Task SignInAsync(SignInContext context)
            {
                // Not supported, fall through
                if (PriorHandler != null)
                {
                    return PriorHandler.SignInAsync(context);
                }

                return Task.FromResult(0);
            }

            public Task SignOutAsync(SignOutContext context)
            {
                // Not supported, fall through
                if (PriorHandler != null)
                {
                    return PriorHandler.SignOutAsync(context);
                }

                return Task.FromResult(0);
            }
        }
    }
}
