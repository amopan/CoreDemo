﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

namespace CoreDemo.IdentityServer.Controller
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Text.Encodings.Web;
    using System.Threading.Tasks;

    using CoreDemo.IdentityServer.Models;

    using IdentityModel;

    using IdentityServer4;
    using IdentityServer4.Models;
    using IdentityServer4.Services;
    using IdentityServer4.Services.InMemory;
    using IdentityServer4.Stores;

    using Microsoft.AspNetCore.Http.Authentication;
    using Microsoft.AspNetCore.Mvc;

    //sample : https://github.com/IdentityServer/IdentityServer4.Samples/blob/dev/Quickstarts/7_JavaScriptClient/src/QuickstartIdentityServer/Controllers/AccountController.cs
    public class AccountController : Controller
    {
        private readonly IClientStore clientStore;

        private readonly IIdentityServerInteractionService interaction;

        private readonly InMemoryUserLoginService loginService;

        public AccountController(
            InMemoryUserLoginService loginService,
            IIdentityServerInteractionService interaction,
            IClientStore clientStore)
        {
            this.loginService = loginService;
            this.interaction = interaction;
            this.clientStore = clientStore;
        }

        /// <summary>
        ///     Show login page
        /// </summary>
        [HttpGet]
        public async Task<IActionResult> Login(string returnUrl)
        {
            var context = await this.interaction.GetAuthorizationContextAsync(returnUrl);
            if (context?.IdP != null)
            {
                // if IdP is passed, then bypass showing the login screen
                return this.ExternalLogin(context.IdP, returnUrl);
            }

            var vm = await this.BuildLoginViewModelAsync(returnUrl, context);

            if (vm.EnableLocalLogin == false && vm.ExternalProviders.Count() == 1)
            {
                // only one option for logging in
                return this.ExternalLogin(vm.ExternalProviders.First().AuthenticationScheme, returnUrl);
            }

            return this.View(vm);
        }

        /// <summary>
        ///     Handle postback from username/password login
        /// </summary>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginInputModel model)
        {
            if (this.ModelState.IsValid)
            {
                // validate username/password against in-memory store
                if (this.loginService.ValidateCredentials(model.Username, model.Password))
                {
                    // issue authentication cookie with subject ID and username
                    var user = this.loginService.FindByUsername(model.Username);

                    AuthenticationProperties props = null;
                    // only set explicit expiration here if persistent. 
                    // otherwise we reply upon expiration configured in cookie middleware.
                    if (model.RememberLogin)
                        props = new AuthenticationProperties
                                {
                                    IsPersistent = true,
                                    ExpiresUtc = DateTimeOffset.UtcNow.AddMonths(1)
                                };

                    await this.HttpContext.Authentication.SignInAsync(user.Subject, user.Username, props);

                    // make sure the returnUrl is still valid, and if yes - redirect back to authorize endpoint
                    if (this.interaction.IsValidReturnUrl(model.ReturnUrl)) return this.Redirect(model.ReturnUrl);

                    return this.Redirect("~/");
                }

                this.ModelState.AddModelError("", "Invalid username or password.");
            }

            // something went wrong, show form with error
            var vm = await this.BuildLoginViewModelAsync(model);
            return this.View(vm);
        }

        private async Task<LoginViewModel> BuildLoginViewModelAsync(string returnUrl, AuthorizationRequest context)
        {
            var providers =
                this.HttpContext.Authentication.GetAuthenticationSchemes()
                    .Where(x => x.DisplayName != null)
                    .Select(
                        x =>
                            new ExternalProvider
                            {
                                DisplayName = x.DisplayName,
                                AuthenticationScheme = x.AuthenticationScheme
                            });

            var allowLocal = true;
            if (context?.ClientId != null)
            {
                var client = await this.clientStore.FindEnabledClientByIdAsync(context.ClientId);
                if (client != null)
                {
                    allowLocal = client.EnableLocalLogin;

                    if ((client.IdentityProviderRestrictions != null) && client.IdentityProviderRestrictions.Any())
                        providers =
                            providers.Where(
                                provider => client.IdentityProviderRestrictions.Contains(provider.AuthenticationScheme));
                }
            }

            return new LoginViewModel
                   {
                       EnableLocalLogin = allowLocal,
                       ReturnUrl = returnUrl,
                       Username = context?.LoginHint,
                       ExternalProviders = providers.ToArray()
                   };
        }

        private async Task<LoginViewModel> BuildLoginViewModelAsync(LoginInputModel model)
        {
            var context = await this.interaction.GetAuthorizationContextAsync(model.ReturnUrl);
            var vm = await this.BuildLoginViewModelAsync(model.ReturnUrl, context);
            vm.Username = model.Username;
            vm.RememberLogin = model.RememberLogin;
            return vm;
        }

        /*/// <summary>
        ///     Show logout page
        /// </summary>
        [HttpGet]
        public async Task<IActionResult> Logout(string logoutId)
        {
            if (this.User.Identity.IsAuthenticated == false) return await this.Logout(new LogoutViewModel { LogoutId = logoutId });

            var context = await this.interaction.GetLogoutContextAsync(logoutId);
            if (context?.ShowSignoutPrompt == false) return await this.Logout(new LogoutViewModel { LogoutId = logoutId });

            // show the logout prompt. this prevents attacks where the user
            // is automatically signed out by another malicious web page.
            var vm = new LogoutViewModel { LogoutId = logoutId };

            return this.View(vm);
        }*/

        /*/// <summary>
        ///     Handle logout page postback
        /// </summary>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout(LogoutViewModel model)
        {
            var idp = this.User?.FindFirst(JwtClaimTypes.IdentityProvider)?.Value;
            if ((idp != null) && (idp != IdentityServerConstants.LocalIdentityProvider))
            {
                if (model.LogoutId == null) model.LogoutId = await this.interaction.CreateLogoutContextAsync();

                var url = "/Account/Logout?logoutId=" + model.LogoutId;
                try
                {
                    // hack: try/catch to handle social providers that throw
                    await
                        this.HttpContext.Authentication.SignOutAsync(
                            idp,
                            new AuthenticationProperties { RedirectUri = url });
                }
                catch (NotSupportedException)
                {
                }
            }

            // delete authentication cookie
            await this.HttpContext.Authentication.SignOutAsync();

            // set this so UI rendering sees an anonymous user
            this.HttpContext.User = new ClaimsPrincipal(new ClaimsIdentity());

            // get context information (client name, post logout redirect URI and iframe for federated signout)
            var logout = await this.interaction.GetLogoutContextAsync(model.LogoutId);

            var vm = new LoggedOutViewModel
                     {
                         PostLogoutRedirectUri = logout?.PostLogoutRedirectUri,
                         ClientName = logout?.ClientId,
                         SignOutIframeUrl = logout?.SignOutIFrameUrl
                     };

            return this.View("LoggedOut", vm);
        }*/

        /// <summary>
        ///     initiate roundtrip to external authentication provider
        /// </summary>
        [HttpGet]
        public IActionResult ExternalLogin(string provider, string returnUrl)
        {
            if (returnUrl != null) returnUrl = UrlEncoder.Default.Encode(returnUrl);
            returnUrl = "/account/externallogincallback?returnUrl=" + returnUrl;

            // start challenge and roundtrip the return URL
            var props = new AuthenticationProperties { RedirectUri = returnUrl, Items = { { "scheme", provider } } };
            return new ChallengeResult(provider, props);
        }

        /// <summary>
        ///     Post processing of external authentication
        /// </summary>
        [HttpGet]
        public async Task<IActionResult> ExternalLoginCallback(string returnUrl)
        {
            // read external identity from the temporary cookie
            var info =
                await
                    this.HttpContext.Authentication.GetAuthenticateInfoAsync(
                        IdentityServerConstants.ExternalCookieAuthenticationScheme);
            var tempUser = info?.Principal;
            if (tempUser == null) throw new Exception("External authentication error");

            // retrieve claims of the external user
            var claims = tempUser.Claims.ToList();

            // try to determine the unique id of the external user - the most common claim type for that are the sub claim and the NameIdentifier
            // depending on the external provider, some other claim type might be used
            var userIdClaim = claims.FirstOrDefault(x => x.Type == JwtClaimTypes.Subject);
            if (userIdClaim == null) userIdClaim = claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);
            if (userIdClaim == null) throw new Exception("Unknown userid");

            // remove the user id claim from the claims collection and move to the userId property
            // also set the name of the external authentication provider
            claims.Remove(userIdClaim);
            var provider = info.Properties.Items["scheme"];
            var userId = userIdClaim.Value;

            // check if the external user is already provisioned
            var user = this.loginService.FindByExternalProvider(provider, userId);
            if (user == null) user = this.loginService.AutoProvisionUser(provider, userId, claims);

            var additionalClaims = new List<Claim>();

            // if the external system sent a session id claim, copy it over
            var sid = claims.FirstOrDefault(x => x.Type == JwtClaimTypes.SessionId);
            if (sid != null) additionalClaims.Add(new Claim(JwtClaimTypes.SessionId, sid.Value));

            // issue authentication cookie for user
            await
                this.HttpContext.Authentication.SignInAsync(
                    user.Subject,
                    user.Username,
                    provider,
                    additionalClaims.ToArray());

            // delete temporary cookie used during external authentication
            await
                this.HttpContext.Authentication.SignOutAsync(IdentityServerConstants.ExternalCookieAuthenticationScheme);

            // validate return URL and redirect back to authorization endpoint
            if (this.interaction.IsValidReturnUrl(returnUrl)) return this.Redirect(returnUrl);

            return this.Redirect("~/");
        }
    }
}