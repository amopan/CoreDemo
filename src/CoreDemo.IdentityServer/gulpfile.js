﻿/// <binding AfterBuild='build:dev-short' />
var gulp = require('gulp');
var clean = require('gulp-clean'),
    concat = require("gulp-concat"),
    cssmin = require("gulp-cssmin"),
    uglify = require("gulp-uglify"),
    ts = require('gulp-typescript');
path = require('path'),
Builder = require('systemjs-builder'),
sourcemaps = require('gulp-sourcemaps'),
less = require('gulp-less');

var paths = {
    root: './'
};

paths.wwwroot = paths.root + "wwwroot/";
paths.libs = paths.wwwroot + "lib/";
paths.app = paths.wwwroot + "app/";
paths.html = paths.wwwroot + "html/";
paths.css = paths.wwwroot + "css/";
paths.fonts = paths.wwwroot + "fonts/";
paths.src = paths.root + "app/";


var config = {
    paths: paths
};


var destPath = './wwwroot/lib/';

// Delete the dist directory
gulp.task('clean:libs', function () {
    return gulp.src(config.paths.libs)
        .pipe(clean());
});
gulp.task('clean:app', function () {
    return gulp.src(config.paths.app)
        .pipe(clean());
});
gulp.task('clean:html', function () {
    return gulp.src(config.paths.html)
        .pipe(clean());
});
gulp.task('clean:css', function () {
    return gulp.src(config.paths.css)
        .pipe(clean());
});
gulp.task('clean', ['clean:libs', 'clean:app', 'clean:html', 'clean:css']);

gulp.task("copy:bootstrap-less-css",
() => {
    var bootstrapCssPatch = config.paths.libs + "bootstrap/css/";
    gulp.src('bootstrap.less',
        {
            cwd: "node_modules/bootstrap-less/bootstrap/**"
        })
        .pipe(sourcemaps.init())
        .pipe(less())
        .pipe(sourcemaps.write("./"))
        .pipe(gulp.dest(bootstrapCssPatch));
});
gulp.task("copy:bootstrap-less-fonts",
() => {
    return gulp.src(["*.*"],
       {
           cwd: "node_modules/bootstrap-less/fonts/**"
       })
       .pipe(gulp.dest(config.paths.libs + "bootstrap/fonts/"));
});
gulp.task("copy:bootstrap-less-js",
() => {
    return gulp.src(["bootstrap.js"],
       {
           cwd: "node_modules/bootstrap-less/js/**"
       })
       .pipe(gulp.dest(config.paths.libs + "bootstrap/js/"));
});
gulp.task("copy:bootstrap", ["copy:bootstrap-less-css", "copy:bootstrap-less-js", "copy:bootstrap-less-fonts"]);
gulp.task("copy:libs", ["copy:bootstrap"],
() => {
    return gulp.src([
            'jquery/dist/jquery.*js',
            'jquery-validation/dist/**',
            'jquery-validation-unobtrusive/jquery.*js',
            'bootstrap/dist/js/bootstrap.*js'
    ], {
        cwd: "node_modules/**"
    })
        .pipe(gulp.dest(config.paths.libs));
});
gulp.task("copy:html",
() => {
    return gulp.src([config.paths.src + "**/*.html"], { base: config.paths.src }).pipe(gulp.dest(config.paths.html));
});
gulp.task("copy:js",
    () => {
        return gulp.src(["*.js"],
            {
                cwd: "ClientContent/Scripts/**"
            })
            .pipe(gulp.dest(config.paths.wwwroot + "js/"));
    });
gulp.task("copy:css",
    () => {
        var bootstrapCssPatch = config.paths.css;
        gulp.src('site.less',
            {
                cwd: "ClientContent/Css/**"
            })
            .pipe(sourcemaps.init())
            .pipe(less())
            .pipe(sourcemaps.write("./"))
            .pipe(gulp.dest(bootstrapCssPatch));
    });

gulp.task('build:dev-short', ['copy:js','copy:css']);
gulp.task('build:dev-full', ['build:dev-short','copy:libs']);

gulp.task('watch:html', function () {

    return gulp.watch(config.paths.src + "**/*.html",
        function (obj) {
            if (obj.type === 'changed') {
                gulp.src(obj.path, { base: config.paths.src })
                    .pipe(gulp.dest(config.paths.html));
            }
        });

});


