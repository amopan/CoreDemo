﻿namespace CoreDemo.IdentityServer.Models
{
    public class LogoutViewModel
    {
        public string LogoutId { get; set; }
    }
}