﻿namespace CoreDemo.IdentityServer.Models
{
    using IdentityServer4.Models;

    public class ErrorViewModel
    {
        public ErrorMessage Error { get; set; }
    }
}