﻿import { Component, OnInit, OnDestroy  } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';

import { User}  from '../models/index';
import {UserService } from "../services/index";
import { Subscription }       from 'rxjs/Subscription';
/**
 * This class represents the lazy loaded UserListComponent.
 */
@Component({
    selector: 'sd-user-list',
    templateUrl: './html/users/user-list/user-list.component.html'
})
export class UserListComponent implements OnInit, OnDestroy {
    public users: User[];

    private selectedId: number;
    private sub: Subscription;

    constructor(
        private service: UserService,
        private route: ActivatedRoute,
        private router: Router) { }

    ngOnInit() {
        this.sub = this.route
            .params
            .subscribe(params => {
                this.selectedId = +params['id'];
                this.service.getAll()
                    .subscribe(users => {
                        this.users = users;
                    });
            });
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    isSelected(user: User) { return user.id === this.selectedId; }

    onSelect(user: User) {
        this.router.navigate(['/users', user.id]);
    }

}