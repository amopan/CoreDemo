﻿import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';
//import { FormsModule }    from '@angular/forms';

import { SharedModule }       from '../shared/shared.module';
import { UsersComponent,UserListComponent, UserEditComponent, UserService, routing }    from './index';

@NgModule({
    imports: [
        CommonModule,
        SharedModule, routing
    ],
    declarations: [
        UsersComponent,
        UserListComponent,
        UserEditComponent
    ],
    providers: [
        UserService
    ]
})
export class UsersModule {
    constructor() {
        console.log("UsersModule created");
    }
}
