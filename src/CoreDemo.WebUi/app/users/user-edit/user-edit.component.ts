﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { FORM_DIRECTIVES, REACTIVE_FORM_DIRECTIVES } from '@angular/forms';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Response } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';

import { User } from '../models/index';
import { UserService } from "../services/index";
import { Subscription } from 'rxjs/Subscription';

import 'rxjs/add/operator/catch';
/**
 * This class represents the lazy loaded UserEditComponent.
 */
@Component({
    selector: 'sd-user-edit',
    templateUrl: './html/users/user-edit/user-edit.component.html',
    directives: [FORM_DIRECTIVES, REACTIVE_FORM_DIRECTIVES]
})
export class UserEditComponent implements OnInit, OnDestroy {

    public static validationMessages = {
        'firstName': {
            'required': "First Name is required."
        },
        'lastName': {
            'required': "Last Name is required."
        },
        'email': {
            'required': "Email is required."
        }
    };

    public userId:number;
    public user: User;
    public error:any;
    private userForm: FormGroup;

    private sub: Subscription;

    constructor(private fb: FormBuilder,
        private service: UserService,
        private route: ActivatedRoute,
        private router: Router) {
        

    }

    
    ngOnInit() {
        this.sub = this.route
            .params
            .subscribe(params => {
                this.userId = +params['id'];
                if (this.userId > 0) {
                    this.service.getById(this.userId)
                        //.catch((error: any) => this.error = error)
                        .subscribe(user => {
                                this.user = user;
                                this.buildForm();
                            },
                        (err: Response | any) => {
                            
                            if (err && err instanceof Response ) {
                                this.error = err.json();
                            } else {
                                this.error = {message:"Requested user not found!"};
                            }
                        });
                } else {
                    this.user = new User();
                    this.buildForm();
                }

            });
    }

    buildForm() {
        this.userForm = this.fb.group({
            firstName: [this.user.firstName, Validators.required],
            lastName: [this.user.lastName, Validators.required],
            email: [this.user.email, Validators.required]
            //roles=[]
        });

    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }
    public doSaveUser() {
        
    }
}