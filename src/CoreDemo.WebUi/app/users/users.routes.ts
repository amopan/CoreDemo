﻿import { Routes, RouterModule } from "@angular/router";
import {RouteMetadatas, RouteMetadata, MenuItemType, RouteService } from "../shared/index";
import {SharedModule} from "../shared/shared.module";

import { UsersComponent, UserListComponent, UserEditComponent } from "./index";


export const routes: RouteMetadatas = [
    {
        name: "Users",
        itemType: MenuItemType.Top,
        path: "users",
        component: UsersComponent,
        children: [
            { path: '', redirectTo: 'list', pathMatch: 'full' },
            {
                name: "User List",
                itemType: MenuItemType.None,
                path: "list",
                component: UserListComponent
            },
            {
                name: "User Edit",
                itemType: MenuItemType.None,
                path: ":id",
                component: UserEditComponent
            }
        ]
    }
];

SharedModule.registerMenuItems(routes);//
console.log('added users routes');

export const routing = RouterModule.forChild(routes);
