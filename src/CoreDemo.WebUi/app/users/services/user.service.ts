﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';

import {User} from "../models/user.model";
import { Configuration,ApiGatewayService} from "../../shared/index";


@Injectable()
export class UserService {

    private usersUrl: string;

    constructor(private http: ApiGatewayService, private configuration: Configuration) {
        this.usersUrl = configuration.apiRoot + "users/";
    }

    getAll(): Observable<User[]> {
        var result = this.http.get(this.usersUrl).map(UserService.mapUsers);
        return result;
    }

    getById(id: number): Observable<User> {
        var result = this.http.get(this.usersUrl + id).map(UserService.mapUser);
        return result;
    }

    static mapUsers(response: Response): User[] {
        if (response && response.json) {
            return response.json().map(UserService.toUser);
        } else return [];
    }

    static mapUser(response: Response): User {
        if (response && response.json) {
            return UserService.toUser(response.json());
        } else {
            return null;
        }
    }

    static toUser(u: any): User {
        var resUser = new User();
        (<any>Object).assign(resUser, u);
        return resUser;
    }
}
