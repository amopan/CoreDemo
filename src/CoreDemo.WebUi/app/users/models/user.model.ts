﻿export class User {
    id: number;
    firstName: string;
    lastName: string;
    email: string;
    roles: Array<string>;

    constructor(id?:number,firstName?:string,lastName?:string,email?:string,roles?:Array<string>) {
        this.id = id;
        this.firstName = firstName;
        this.lastName= lastName;
        this.email = email;
        this.roles= roles?roles:[];
    }
    
}