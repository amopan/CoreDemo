﻿/**
* This barrel file provides the export for the lazy loaded Users module types.
*/
export * from "./models/index";
export * from "./services/index";
export * from "./user-list/index";
export * from "./user-edit/index";
export * from "./users.component";

export * from "./users.routes";