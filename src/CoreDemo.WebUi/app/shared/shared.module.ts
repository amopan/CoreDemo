﻿import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from "@angular/router";
import { CommonModule }        from '@angular/common';
import { FormsModule, ReactiveFormsModule }         from '@angular/forms';
import { BrowserModule }  from '@angular/platform-browser';

import {RouteService, RouteMetadatas, ALL_ROUTES, Configuration, AuthDataService, SecurityService,ApiGatewayService,HttpErrorHandlerService} from "./index";

//export const routeList: RouteMetadatas = [];

@NgModule({
    imports: [RouterModule, CommonModule, FormsModule,
        ReactiveFormsModule],
    exports: [RouterModule, CommonModule, FormsModule,
        ReactiveFormsModule],
    providers: [Configuration, SecurityService, AuthDataService, ApiGatewayService, HttpErrorHandlerService]
})
export class SharedModule {
    constructor() {
        console.log("SharedModule created");
    }
    static registerMenuItems(routes: RouteMetadatas)/*: ModuleWithProviders*/ {
        RouteService.setupRoutes(routes);
        //return RouterModule.forChild(routes);
    }
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [RouteService]
        };
    }
}

