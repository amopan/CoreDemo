﻿import { Injectable } from '@angular/core';
import { Routes,RouterModule  } from "@angular/router";
import { RouteMetadata, RouteMetadatas, MenuItemType } from "./router.metadata";

export const ALL_ROUTES: RouteMetadatas = [];

@Injectable()
export class RouteService {
    //static allRoutes: RouteMetadatas=[];

    constructor() {
        console.log("new RouteService");
        //this.setupSharedRoutes();
    }

    
    public getRoutes(itemType?:MenuItemType): RouteMetadatas {
        
        return ALL_ROUTES;
    }
    static setupRoutes(routes: RouteMetadatas) {
        ALL_ROUTES.push(...routes);
        //RouteService.allRoutes=RouteService.allRoutes.concat(routes);
    }
}