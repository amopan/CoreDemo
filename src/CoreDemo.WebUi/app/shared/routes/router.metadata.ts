﻿import {Route} from "@angular/router"

export enum MenuItemType {
    None,
    Left,
    Top,
    Brand
}

export interface RouteMetadata extends Route {
    name?: string;
    itemType?: MenuItemType;
    children?: RouteMetadata[];
}

export class RouteMetadatas extends Array<RouteMetadata> {
    constructor() {
        super();
        console.info("RouteMetadatas created");
    }
}