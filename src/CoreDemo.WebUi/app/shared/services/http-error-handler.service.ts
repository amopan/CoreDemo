﻿
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import {AuthDataService} from "./auth-data.service";

import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/of';
@Injectable()
export class HttpErrorHandlerService {

    constructor(private router: Router, private authDataService: AuthDataService) { }
    public lastError(): any {
        return "";//todo: implement last error
    }
    public handle(error: any):Observable<any> {
        console.log("HttpErrorHandlerService.handle: "+ error);
        if (error.status === 403) {
            this.router.navigate(['/forbidden']);
            return Observable.of([]);
        }
        else if (error.status === 401) {
            this.authDataService.resetAuthorizationData();
            this.router.navigate(['/unauthorized']);
            return Observable.of([]);
        } else {
            return Observable.throw(error);
        }
    }
}