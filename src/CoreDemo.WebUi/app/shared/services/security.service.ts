﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { Configuration} from "../app.configuration";
import {ApiGatewayService} from "./api-gateway.service"
import {AuthDataService} from "./auth-data.service";
@Injectable()
export class SecurityService {

    private headers: Headers;




    constructor(private authDataService: AuthDataService, private http: ApiGatewayService, private configuration: Configuration, private router: Router) {


        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
    }

    public authorize() {
        this.authDataService.resetAuthorizationData();

        console.log("BEGIN Authorize, no auth data");
        var authConfig =this.configuration.authenticationConfig;
        var authorizationUrl =authConfig.authenticationServerUrl+ authConfig.authorizationUri;
        var clientId = authConfig.clientId;
        var redirectUri = this.configuration.hostUri+authConfig.redirectUrl;
        var responseType = authConfig.responseType;
        var scope = authConfig.scope;
        var nonce = "N" + Math.random() + "" + Date.now();
        var state = Date.now() + "" + Math.random();

        this.authDataService.store("authStateControl", state);
        this.authDataService.store("authNonce", nonce);
        console.log("AuthorizedController created. adding myautostate: " + this.authDataService.retrieve("authStateControl"));

        var url =
            authorizationUrl + "?" +
            "response_type=" + encodeURI(responseType) + "&" +
            "client_id=" + encodeURI(clientId) + "&" +
            "redirect_uri=" + encodeURI(redirectUri) + "&" +
            "scope=" + encodeURI(scope) + "&" +
            "nonce=" + encodeURI(nonce) + "&" +
            "state=" + encodeURI(state);

        window.location.href = url;
    }

    public authorizedCallback() {
        console.log("BEGIN AuthorizedCallback, no auth data");
        this.authDataService.resetAuthorizationData();

        var hash = decodeURIComponent(window.location.hash.substr(1));

        var result: any = hash.split('&').reduce((result: any, item: string) => {
            var parts = item.split('=');
            result[parts[0]] = parts[1];
            return result;
        }, {});

        console.log(result);
        console.log("AuthorizedCallback created, begin token validation");

        var token = "";
        var idToken = "";
        var authResponseIsValid = false;
        if (!result.error) {

            if (result.state !== this.authDataService.retrieve("authStateControl")) {
                console.log("AuthorizedCallback incorrect state");
            } else {

                token = result.access_token;
                idToken = result.id_token;

                var dataIdToken: any = this.authDataService.getDataFromToken(idToken);
                console.log(dataIdToken);

                // validate nonce
                if (dataIdToken.nonce !== this.authDataService.retrieve("authNonce")) {
                    console.log("AuthorizedCallback incorrect nonce");
                } else {
                    this.authDataService.store("authNonce", "");
                    this.authDataService.store("authStateControl", "");

                    authResponseIsValid = true;
                    console.log("AuthorizedCallback state and nonce validated, returning access token");
                }
            }
        }

        if (authResponseIsValid) {
            this.authDataService.setAuthorizationData(token, idToken);
            console.log(this.authDataService.retrieve("authorizationData"));

            // router navigate to DataEventRecordsList
            this.router.navigate(['/']);
        }
        else {
            this.authDataService.resetAuthorizationData();
            this.router.navigate(['/unauthorized']);
        }
    }

    public logoff() {
        // /connect/endsession?id_token_hint=...&post_logout_redirect_uri=https://myapp.com
        console.log("BEGIN Authorize, no auth data");
        var authConfig = this.configuration.authenticationConfig;
        var authorizationUrl = authConfig.authenticationServerUrl+'connect/endsession';

        var idTokenHint = this.authDataService.retrieve("authorizationDataIdToken");
        var postLogoutRedirectUri = this.configuration.hostUri + authConfig.postLogoutRedirectUrl;

        var url =
            authorizationUrl + "?" +
            "id_token_hint=" + encodeURI(idTokenHint) + "&" +
            "post_logout_redirect_uri=" + encodeURI(postLogoutRedirectUri);

        this.authDataService.resetAuthorizationData();

        window.location.href = url;
    }

    
}