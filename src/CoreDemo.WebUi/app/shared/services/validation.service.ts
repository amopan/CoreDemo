﻿export class ValidationResult {
    [key: string]: any;
}
export class ValidationService {
    static emailValidator(control): ValidationResult
    {
        
        // RFC 2822 compliant regex
        if (control.value && !control.value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/))
        {
            return { 'invalidEmailAddress': true };
        }
        return null;

    }
}