﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { Configuration} from "../app.configuration";
@Injectable()
export class AuthDataService {

    private storage: any;
    public isAuthorized: boolean;


    constructor() {


        this.storage = sessionStorage; //localStorage;

        if (this.retrieve("isAuthorized") !== "") {
            this.isAuthorized = this.retrieve("isAuthorized");
        }
    }

    public getToken(): any {
        return this.retrieve("authorizationData");
    }

    public resetAuthorizationData() {
        this.store("authorizationData", "");
        this.store("authorizationDataIdToken", "");

        this.isAuthorized = false;
        this.store("isAuthorized", false);
    }
    public setAuthorizationData(token: any, idToken: any) {
        if (this.retrieve("authorizationData") !== "") {
            this.store("authorizationData", "");
        }

        this.store("authorizationData", token);
        this.store("authorizationDataIdToken", idToken);
        this.isAuthorized = true;
        this.store("isAuthorized", true);

        var data: any = this.getDataFromToken(idToken);
        if (data.roles) {
            for (var i = 0; i < data.roles.length; i++) {

            }
        }
    }
    
    public  getDataFromToken(token: any) {
        var data = {};
        if (typeof token !== 'undefined' && token) {
            var encoded = token.split('.')[1];
            data = JSON.parse(this.urlBase64Decode(encoded));
        }

        return data;
    }

    private urlBase64Decode(str: string) {
        var output = str.replace('-', '+').replace('_', '/');
        switch (output.length % 4) {
            case 0:
                break;
            case 2:
                output += '==';
                break;
            case 3:
                output += '=';
                break;
            default:
                throw 'Illegal base64url string!';
        }

        return window.atob(output);
    }


    public  retrieve(key: string): any {
        var item = this.storage.getItem(key);

        if (item && item !== 'undefined') {
            return JSON.parse(this.storage.getItem(key));
        }

        return null;
    }

    public  store(key: string, value: any) {
        this.storage.setItem(key, JSON.stringify(value));
    }
}