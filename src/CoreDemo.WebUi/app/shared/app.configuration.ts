﻿import { Injectable } from '@angular/core';

export class AuthenticationConfiguration {
    public authenticationServerUrl: string = "http://localhost:4446/";
    public authorizationUri:string ="connect/authorize";
    public clientId: string = "coredemo-client";
    public scope: string = "coredemo-api openid";
    public responseType: string = "id_token token";
    public redirectUrl: string = "/";
    public postLogoutRedirectUrl: string = "/unauthorized";
}

@Injectable()
export class Configuration {
    public hostUri: string ="http://localhost:4448";
    public apiRoot: string = "http://localhost:4447/api/";
    public authenticationConfig: AuthenticationConfiguration = new AuthenticationConfiguration();
}