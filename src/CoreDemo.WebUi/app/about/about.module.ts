﻿import { NgModule } from '@angular/core';

import { SharedModule }       from '../shared/shared.module';
import { AboutComponent, routing }     from './index';

@NgModule({
    imports: [SharedModule, routing],
    //providers: [],
    declarations: [AboutComponent]
})
export class AboutModule { }