﻿import { Routes,
    RouterModule } from "@angular/router";
import {RouteMetadatas, RouteMetadata, MenuItemType, RouteService } from "../shared/index";
import {SharedModule} from "../shared/shared.module";

import { AboutComponent } from "./index";



export const aboutRoutes1: RouteMetadatas = [
    {
        name: "About",
        itemType: MenuItemType.Top,
        path: "about",
        component: AboutComponent
    }
];



SharedModule.registerMenuItems(aboutRoutes1);//
console.log('added about routes');

export const routing = RouterModule.forChild(aboutRoutes1);
