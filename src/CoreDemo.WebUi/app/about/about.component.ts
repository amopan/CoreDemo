﻿import { Component } from '@angular/core';

/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
    selector: 'sd-about',
    templateUrl: './html/about/about.component.html'/*,
    styleUrls: ['./css/about.component.css']*/
})
export class AboutComponent { }