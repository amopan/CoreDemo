﻿import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { UnauthorizedComponent, ForbiddenComponent, NotFoundComponent, routing } from './index';

@NgModule({
    imports: [SharedModule, routing],
    declarations: [UnauthorizedComponent, ForbiddenComponent, NotFoundComponent]
})
export class ErrorsModule {
    constructor() {
        console.log("ErrorsModule created");
    }
}