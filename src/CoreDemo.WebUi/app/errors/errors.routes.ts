﻿import {
    Routes,
    RouterModule
} from "@angular/router";
import { RouteMetadatas, RouteMetadata, MenuItemType, RouteService } from "../shared/index";
import { SharedModule } from "../shared/shared.module";

import { NotFoundComponent, UnauthorizedComponent, ForbiddenComponent, } from "./index";



export const notFoundRoutes: RouteMetadatas = [
    {
        name: "NotFound",
        itemType: MenuItemType.None,
        path: "notfound",
        component: NotFoundComponent
    } ,
    {
        name: "Unauthorized",
        itemType: MenuItemType.None,
        path: "unauthorized",
        component: UnauthorizedComponent
    },

    {
        name: "Forbidden",
        itemType: MenuItemType.None,
        path: "forbidden",
        component: ForbiddenComponent
    }
];



SharedModule.registerMenuItems(notFoundRoutes);//
console.log('added not-found routes');

export const routing = RouterModule.forChild(notFoundRoutes);
