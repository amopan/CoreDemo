﻿import { Component } from '@angular/core';

import { HttpErrorHandlerService } from "../../shared/index";

@Component({
    selector: 'not-found',
    templateUrl: './html/errors/not-found/not-found.component.html'
})
export class NotFoundComponent {
public message:string;
    constructor(private errorHandlerService: HttpErrorHandlerService) {
        let lastError = errorHandlerService.lastError();

        //todo: extract message more properly
        this.message = lastError ? lastError.json().message : "";
    }
}