﻿import { Routes, RouterModule } from "@angular/router";
import {RouteMetadatas, RouteMetadata, MenuItemType, RouteService } from "../shared/index";
import {SharedModule} from "../shared/shared.module";

import { UnauthorizedComponent, ForbiddenComponent, LoginComponent  } from "./index";


export const routes: RouteMetadatas = [
    {
        name: "Login",
        itemType: MenuItemType.None,
        path: "login",
        component: LoginComponent
    },
    {
        name: "Unauthorized",
        itemType: MenuItemType.None,
        path: "unauthorized",
        component: UnauthorizedComponent
    },
   
    {
        name: "Forbidden",
        itemType: MenuItemType.None,
        path: "forbidden",
        component: ForbiddenComponent
    }

];

SharedModule.registerMenuItems(routes);//
console.log('added account routes');

export const routing = RouterModule.forChild(routes);
