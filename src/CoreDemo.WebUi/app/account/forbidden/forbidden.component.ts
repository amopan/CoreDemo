﻿import { Component, OnInit } from '@angular/core';
//import { REACTIVE_FORM_DIRECTIVES } from '@angular/forms';
import { SecurityService } from "../../shared/index";
@Component({
    //selector: 'sd-forbidden',
    templateUrl: './html/account/forbidden/forbidden.component.html'
})
export class ForbiddenComponent implements OnInit {

    errorMessage: string;
    constructor(private securityService: SecurityService) { }
    ngOnInit() {

    }
    public login() {
        this.securityService.authorize();
    }

}