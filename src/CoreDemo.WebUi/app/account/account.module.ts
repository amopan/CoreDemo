﻿import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';
import { FormsModule }    from '@angular/forms';

import { SharedModule }       from '../shared/shared.module';
import { UnauthorizedComponent, ForbiddenComponent, LoginComponent, routing }    from './index';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        SharedModule, routing
    ],
    declarations: [
        UnauthorizedComponent, LoginComponent, ForbiddenComponent
    ],
    providers: []
})
export class AccountModule {
    constructor() {
        console.log("AccountModule created");
    }
}
