﻿import { Component, OnInit } from '@angular/core';
import { REACTIVE_FORM_DIRECTIVES } from '@angular/forms';
import {SecurityService} from "../../shared/index";
@Component({
    selector: 'sd-unathorized',
    templateUrl: './html/account/unauthorized/unauthorized.component.html'
})
export class UnauthorizedComponent implements OnInit {

    errorMessage: string;
    constructor(private securityService: SecurityService) { }
    ngOnInit() {

    }
    public login() {
        this.securityService.authorize();
    }

}