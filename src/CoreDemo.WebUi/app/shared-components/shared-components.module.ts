﻿import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from "@angular/router";
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';
import { BrowserModule }  from '@angular/platform-browser';
import { SharedModule }  from '../shared/shared.module';

import { NavbarComponent, HeaderComponent} from "./index";

@NgModule({

    imports: [
        //CommonModule,
        //FormsModule,
        //BrowserModule,
        SharedModule
    ],
    declarations: [NavbarComponent, HeaderComponent],
    exports: [NavbarComponent, HeaderComponent]
})
export class SharedComponentsModule {
    constructor() {
        console.log("SharedComponentsModule created");
    }
}

