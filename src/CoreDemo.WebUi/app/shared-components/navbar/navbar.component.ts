﻿import {Component, OnInit, Output, Inject } from "@angular/core"

import { ROUTER_DIRECTIVES} from "@angular/router";

import { RouteMetadata, RouteMetadatas, MenuItemType, RouteService} from "../../shared/index";

import {SecurityService} from "../../shared/index";

@Component({
    selector: "navbar",
    templateUrl: "./html/shared-components/navbar/navbar.component.html",
    directives: [ROUTER_DIRECTIVES]
})

export class NavbarComponent implements OnInit  {
    public menuItems: RouteMetadatas;
    public isAuthorized: boolean;
    public currentUser:any;
    constructor(private routeService: RouteService, private securityService: SecurityService) {
        console.log("new navbar.component");
        this.isAuthorized = false;//securityService.isAuthorized;
    }
    ngOnInit() {
        this.menuItems = this.routeService.getRoutes().filter(menuItem => menuItem.itemType === MenuItemType.Top);
        console.log(this.menuItems.length);
    }

    public getMenuItemClasses(menuItem: RouteMetadata) {
        let menuItemClass = {};
        return menuItemClass;
    }

    public getMenuItemAnchorClasses(menuItem: RouteMetadata) {
        let menuItemAnchorClass = {};
        return menuItemAnchorClass;
    }

    public SignIn() {
        
    }
}
