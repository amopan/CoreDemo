﻿import { Component } from "@angular/core";

import { NavbarComponent } from "../navbar/index";

@Component({
    selector: "site-header",
    templateUrl: "./html/shared-components/header/header.component.html",
    directives:[NavbarComponent]
})
export class HeaderComponent {
    constructor() {
        console.log("new HeaderComponent");
    }
}