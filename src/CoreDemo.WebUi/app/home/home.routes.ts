﻿import {
    Routes,
    RouterModule  } from "@angular/router";
import { RouteMetadatas, RouteMetadata, MenuItemType } from "../shared/index";
import {SharedModule} from "../shared/shared.module";

import {HomeComponent}  from "./home.component";


export const homeRoutes1: RouteMetadatas = [
    {
        name: "Home",
        itemType: MenuItemType.Brand,
        path: "home",
        component: HomeComponent
    }
];

SharedModule.registerMenuItems(homeRoutes1);//RouterModule.forChild(homeRoutes);
console.log('added home routes');
export const routing = RouterModule.forChild(homeRoutes1);
