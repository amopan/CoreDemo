﻿import { NgModule } from '@angular/core';

import { SharedModule }       from '../shared/shared.module';
import { HomeComponent, routing }      from './index';

@NgModule({
    imports: [SharedModule, routing],
    //providers: [],
    declarations: [HomeComponent]
})
export class HomeModule { }