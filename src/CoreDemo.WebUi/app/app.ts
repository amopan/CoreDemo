﻿import {Component} from "@angular/core";
import { ROUTER_DIRECTIVES }  from '@angular/router';

import { SecurityService } from './shared/index';
@Component({
    selector: "my-app",
    templateUrl: "./html/master.html",
    directives: [ROUTER_DIRECTIVES]
    //providers: [RouteService]
    
})

export class AppComponent {
    mySkill: string;
    skills = ["ASP.NET Core 1.0", "Angular2", "C#", "SQL", "JSON"];

    constructor(public securityService: SecurityService) {
        console.log("new AppComponent");
        this.mySkill = this.skills[1];
    }

    ngOnInit() {
        console.log("ngOnInit _securityService.AuthorizedCallback");

        if (window.location.hash) {
            this.securityService.authorizedCallback();
        }
    }

    public login() {
        console.log("Do login logic");
        this.securityService.authorize();
    }

    public logout() {
        console.log("Do logout logic");
        this.securityService.logoff();
    }
}