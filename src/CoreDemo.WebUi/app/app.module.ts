﻿import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
//import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/* App Root */
import { AppComponent }   from './app';


/* Feature Modules */
import { SharedModule }   from './shared/shared.module';
import { SharedComponentsModule }   from './shared-components/shared-components.module';
import { HomeModule }  from './home/home.module';
import { AccountModule }   from './account/account.module';
import { UsersModule }   from './users/users.module';
import { AboutModule }  from './about/about.module';

import { routing }        from './app.routes';



@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        routing,
        SharedComponentsModule,
        AccountModule,
        HomeModule,
        UsersModule,
        AboutModule,
        SharedModule.forRoot()
    ],
    declarations: [AppComponent],

    bootstrap: [AppComponent]
})
export class AppModule { }
