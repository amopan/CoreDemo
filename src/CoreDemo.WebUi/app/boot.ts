﻿///<reference path="./../typings/index.d.ts"/>
import { APP_BASE_HREF } from "@angular/common";
import { disableDeprecatedForms, provideForms } from "@angular/forms";
import { enableProdMode } from "@angular/core";
import {bootstrap, platformBrowserDynamic}    from "@angular/platform-browser-dynamic";

import {AppModule} from "./app.module";

//if ('<%= ENV %>' === 'prod') { enableProdMode(); }
platformBrowserDynamic().bootstrapModule(AppModule);
//angular modules
//https://angular.io/docs/ts/latest/guide/ngmodule.html