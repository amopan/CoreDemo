﻿namespace CoreDemo.WebUi
{
    using System.IO;
    using System.Net;

    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.FileProviders;
    using Microsoft.Extensions.Logging;

    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder =
                new ConfigurationBuilder().SetBasePath(env.ContentRootPath)
                    .AddJsonFile("appsettings.json", true, true)
                    .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true);

            builder.AddEnvironmentVariables();
            this.Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; set; }

        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddGlimpse();//The Diagnostics platform of the web
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole();
            loggerFactory.AddDebug();

            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseMvcWithDefaultRoute();
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();

                app.UseFileServer(
                    new FileServerOptions
                    {
                        FileProvider =new PhysicalFileProvider(Path.Combine(env.ContentRootPath, "app")),
                        RequestPath = new PathString("/src/app")
                    });
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.Use(async (context, next) =>
            {
                await next();
                if (context.Response.StatusCode == 404)
                {
                    if (!context.Request.Path.StartsWithSegments("/lib")
                        && !context.Request.Path.StartsWithSegments("/app")
                        && !context.Request.Path.StartsWithSegments("/html"))
                    {
                        context.Request.Path = "/Home";
                        context.Response.StatusCode = 200;
                    }
                    await next();
                }
            });
            app.UseMvc(
                routes =>
                {
                    routes.MapRoute("default", "{controller=Home}/{action=Index}/{id?}");
                    routes.MapRoute("defaultApi", "api/{controller}/{id?}");
                    //routes.MapRoute("html-not-found", "html/{*anything}",);
                    //routes.MapRoute("spa-fallback", "{*anything}", new { controller = "Home", action = "Index" });
                });
            /*app.Use(async (context, next) =>
            {
                await next();

                if (context.Response.StatusCode == 404 && !Path.HasExtension(context.Request.Path.Value))
                {
                    context.Request.Path = "/index.html"; // Put your Angular root page here 
                    await next();
                }
            });
            */
        }
    }
}