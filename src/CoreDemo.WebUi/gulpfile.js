/// <binding AfterBuild='build:dev-short' />
var gulp = require('gulp');
//var clean = require('gulp-clean'),
    concat = require("gulp-concat"),
    cssmin = require("gulp-cssmin"),
    uglify = require("gulp-uglify"),
    ts = require('gulp-typescript');
path = require('path'),
del = require('del'),
    Builder = require('systemjs-builder'),
    sourcemaps = require('gulp-sourcemaps'),
    less = require('gulp-less');

var paths = {
    root: './'
};

paths.wwwroot = paths.root + "wwwroot/";
paths.libs = paths.wwwroot+ "lib/";
paths.app = paths.wwwroot+ "app/";
paths.html = paths.wwwroot + "html/";
paths.css = paths.wwwroot + "css/";
paths.fonts = paths.wwwroot + "fonts/";
paths.src = paths.root + "app/";


var config = {
    paths: paths
};


var destPath = './wwwroot/lib/';

// Delete the dist directory
gulp.task('clean:libs', function () {
    return del([config.paths.libs+ '**/*']);
    /*return gulp.src(config.paths.libs)
        .pipe(clean());*/
});
gulp.task('clean:app', function () {
    return del([config.paths.app+ '**/*']);
    /*return gulp.src(config.paths.app)
        .pipe(clean());*/
});
gulp.task('clean:html', function () {
    return del([config.paths.html+'**/*']);
    
});
gulp.task('clean', ['clean:libs', 'clean:app', 'clean:html']);
gulp.task("scriptsNStyles", () => {
    return gulp.src([
            'core-js/client/**',
            'systemjs/dist/system.src.js',
            'reflect-metadata/**',
            'rxjs/**',
            'symbol-observable/lib//**',
            'zone.js/dist/**',
            '@angular/**',
            'angular2-in-memory-web-api/**/*.js',
            'jquery/dist/jquery.*js',
            'bootstrap/dist/js/bootstrap.*js'
        ],
        {
            cwd: "node_modules/**"
        })
        .pipe(gulp.dest(config.paths.libs));
});

gulp.task("copy:bootstrap-less-css",
() => {
    var bootstrapCssPatch = config.paths.libs + "bootstrap/css/";
    gulp.src('bootstrap.less',
        {
            cwd: "node_modules/bootstrap-less/bootstrap/**"
        })
        .pipe(sourcemaps.init())
        .pipe(less())
        .pipe(sourcemaps.write("./"))
        .pipe(gulp.dest(bootstrapCssPatch));
});
gulp.task("copy:bootstrap-less-fonts",
() => {
    return gulp.src(["*.*"],
       {
           cwd: "node_modules/bootstrap-less/fonts/**"
       })
       .pipe(gulp.dest(config.paths.libs+"bootstrap/fonts/"));
});
gulp.task("copy:bootstrap-less-js",
() => {
    return gulp.src(["bootstrap.js"],
       {
           cwd: "node_modules/bootstrap-less/js/**"
       })
       .pipe(gulp.dest(config.paths.libs + "bootstrap/js/"));
});
gulp.task("copy:bootstrap", ["copy:bootstrap-less-css", "copy:bootstrap-less-js", "copy:bootstrap-less-fonts"]);
gulp.task("copy:libs",["copy:bootstrap"],
() => {
    return gulp.src([
            'core-js/client/**',
            'systemjs/dist/system.src.js',
            'reflect-metadata/**',
            'rxjs/**',
            'symbol-observable/lib//**',
            'zone.js/dist/**',
            '@angular/**',
            'angular2-in-memory-web-api/**/*.js',
            'jquery/dist/jquery.*js',
            'bootstrap/dist/js/bootstrap.*js'
    ], {
        cwd: "node_modules/**"
    })
        .pipe(gulp.dest(config.paths.libs));
});
gulp.task("copy:html",
() => {
    return gulp.src([config.paths.src + "**/*.html"], { base: config.paths.src}).pipe(gulp.dest(config.paths.html));
});

/*var tsProject = ts.createProject('app/tsconfig.json');
gulp.task('ts', function (done) {
    //var tsResult = tsProject.src()
    var tsResult = gulp.src([
            "app/*.ts"
    ])
        .pipe(ts(tsProject), undefined, ts.reporter.fullReporter());
    return tsResult.js.pipe(gulp.dest('./wwwroot/app'));
});

gulp.task('watch', ['watch.ts']);

gulp.task('watch.ts', ['ts'], function () {
    return gulp.watch('app/*.ts', ['ts']);
});*/
gulp.task('build:dev-short', ['copy:html']);
gulp.task('build:dev-full',['build:dev-short', 'copy:libs']);

gulp.task('watch:html', function () {

    return gulp.watch(config.paths.src + "**/*.html",
        function(obj) {
            //if (obj.type === 'changed')
            {
                gulp.src(obj.path, { base: config.paths.src })
                    .pipe(gulp.dest(config.paths.html));
            }
        });

});


