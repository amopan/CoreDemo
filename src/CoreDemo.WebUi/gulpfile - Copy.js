﻿/// <binding AfterBuild='debug' Clean='clean' />
/*
This file in the main entry point for defining Gulp tasks and using Gulp plugins.
Click here to learn more. http://go.microsoft.com/fwlink/?LinkId=518007
*/

var gulp = require("gulp");

rimraf = require("rimraf"),
concat = require("gulp-concat"),
cssmin = require("gulp-cssmin"),
uglify = require("gulp-uglify"),
ts = require('gulp-typescript');

var paths = {

};
paths.webroot = "wwwroot/";
paths.npmSrc = "./node_modules/";
paths.app = paths.webroot + "app/",
paths.libs = paths.webroot + "lib/",
paths.js = paths.webroot + "js/**/*.js";
paths.minJs = paths.webroot + "js/**/*.min.js";
paths.css = paths.webroot + "css/**/*.css";
paths.minCss = paths.webroot + "css/**/*.min.css";
paths.concatJsDest = paths.webroot + "js/site.min.js";
paths.concatCssDest = paths.webroot + "css/site.min.css";

var config = {
    jsItems: {
        'jquery': {
            src: [paths.npmSrc + '/jquery/dist/**/*.*'],
            dest: paths.libs + '/jquery/',
            opt: { base: paths.npmSrc + '/jquery/dist' }
        },
        'systemjs': {
            src: [paths.npmSrc + '/systemjs/dist/**/*.*'],
            dest: paths.libs + '/systemjs/',
            opt: { base: paths.npmSrc + '/systemjs/dist/' }
        },
        'angular2': {
            src: [paths.npmSrc + '/angular2/bundles/**/*.js'],
            dest: paths.libs + '/angular2/',
            opt: { base: paths.npmSrc + '/angular2/bundles/' }
        },
        'es6-shim': {
            src: [paths.npmSrc + '/es6-shim/es6-sh*'],
            dest: paths.libs + '/es6-shim/',
            opt: { base: paths.npmSrc + '/es6-shim/' }
        },
        'es6-promise': {
            src: [paths.npmSrc + '/es6-promise/dist/**/*.*'],
            dest: paths.libs + '/es6-promise/',
            opt: { base: paths.npmSrc + '/es6-promise/dist/' }
        },
        'rxjs': {
            src: [paths.npmSrc + '/rxjs/bundles/*.*'],
            dest: paths.libs + '/rxjs/',
            opt: { base: paths.npmSrc + '/rxjs/bundles/' }
        }


    }
}
var copyDepsTasks = [];
function createTask(key) {
    var taskName = "copy-deps:" + key;
    var jsItem = config.jsItems[key];
    gulp.task(taskName, function () {
        return gulp.src(jsItem.src, jsItem.opt)
         .pipe(gulp.dest(jsItem.dest));;
    });
    copyDepsTasks.push(taskName);
};

for (var key in config.jsItems) {
    createTask(key);
}



gulp.task("copy-deps:bootstrap", function () {
    return gulp.src(paths.npmSrc + '/bootstrap/dist/**/*.*', { base: paths.npmSrc + '/bootstrap/dist' })
         .pipe(gulp.dest(paths.libs + '/bootstrap/'));
});

gulp.task("copy-deps:jsItems", copyDepsTasks);
gulp.task("copy-deps", ["copy-deps:jsItems", "copy-deps:bootstrap"]);
gulp.task("clean:js", function (cb) {
    rimraf(paths.app, cb);
});
gulp.task("clean:lib", function (cb) {
    rimraf(paths.libs, cb);
});

gulp.task("clean:css", function (cb) {
    rimraf(paths.concatCssDest, cb);
});

gulp.task("clean", ["clean:js", "clean:lib", "clean:css"]);

gulp.task("min:js", function () {
    return gulp.src([paths.js, "!" + paths.minJs], { base: "." })
      .pipe(concat(paths.concatJsDest))
      .pipe(uglify())
      .pipe(gulp.dest("."));
});

gulp.task("min:css", function () {
    return gulp.src([paths.css, "!" + paths.minCss])
      .pipe(concat(paths.concatCssDest))
      .pipe(cssmin())
      .pipe(gulp.dest("."));
});

gulp.task("min", ["min:js", "min:css"]);



var tsProject = ts.createProject('app/tsconfig.json');
gulp.task('ts', function (done) {
    //var tsResult = tsProject.src()
    var tsResult = gulp.src([
            "app/**/*.ts"
    ])
        .pipe(ts(tsProject), undefined, ts.reporter.fullReporter());
    return tsResult.js.pipe(gulp.dest(paths.app));
});

gulp.task('watch', ['watch.ts']);

gulp.task('watch.ts', ['ts'], function () {
    return gulp.watch('app/**/*.ts', ['ts']);
});


gulp.task('debug', ['clean', 'copy-deps', 'ts']);