﻿CREATE TABLE [dbo].[User]
(
	[Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[FirstName] NVARCHAR(255) NOT null,
	[LastName] NVARCHAR(255) NOT null,
	[Email] NVARCHAR(255) NOT null
)
