﻿IF NOT EXISTS ( SELECT  Id
                FROM    dbo.[User] )
    BEGIN 
        MERGE INTO dbo.[User] AS Target
        USING ( VALUES
            ( 'First1' ,
              'Last1' ,
              'First1.Last1@mail.com'
            ),
            ( 'First2' ,
              'Last2' ,
              'First2.Last2@mail.com'
            ) ) AS Source ( FirstName, LastName, Email )
        ON ( Target.Email = Source.[Email] )
        WHEN NOT MATCHED BY TARGET THEN
            INSERT ( FirstName ,
                     LastName ,
                     Email
                   )
            VALUES ( Source.FirstName ,
                     Source.LastName ,
                     Source.Email
                   )
        WHEN MATCHED THEN
            UPDATE SET
                    FirstName = Source.LastName ,
                    LastName = Source.LastName ,
                    Email = Source.Email;
    END;
