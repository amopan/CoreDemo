﻿

namespace CoreDemo.Services.Configuration
{
    using Autofac;

    using CoreDemo.Common.CQRS;

    public class ServicesModule:Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            this.ServicesBinding(builder);
            base.Load(builder);
        }

        
        private void ServicesBinding(ContainerBuilder builder)
        {
        
            builder.RegisterGeneric(typeof(EntityServiceAsync<,>)).As(typeof(IEntityServiceAsync<,>)).InstancePerLifetimeScope();
        }
    }
}
