﻿namespace CoreDemo.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    using CoreDemo.Business.CommandModels;
    using CoreDemo.Common.CQRS;
    using CoreDemo.Common.DataAccess;
    using CoreDemo.Common.Entities;

    public class EntityServiceAsync<TEntity, TKey> : IEntityServiceAsync<TEntity, TKey>
        where TEntity : class, IEntity<TKey>
    {
        private readonly ICommandDispatcher commandDispatcher;

        private readonly IUnitOfWorkAsync unitOfWork;

        public EntityServiceAsync(ICommandDispatcher commandDispatcher, IUnitOfWorkAsync unitOfWork)
        {
            this.commandDispatcher = commandDispatcher;
            this.unitOfWork = unitOfWork;
        }

        public async Task<int> DeleteAsync(TEntity entity)
        {
            var deleteModel = new DeleteEntityCommandModel<TEntity, TKey>(entity);

            await
                this.commandDispatcher.DispatchAsync<DeleteEntityCommandModel<TEntity, TKey>, TEntity, TKey, TKey>(
                    deleteModel);

            var res = await this.unitOfWork.ApplyChangesAsync();
            return res;
        }

        public async Task<TEntity> GetAsync<TModel>(TModel model) where TModel : IQueryCommmandModel<TEntity, TKey>
        {
            var result =
                await
                    this.commandDispatcher.DispatchAsync<IQueryCommmandModel<TEntity, TKey>, TEntity, TKey, TEntity>(
                        model);
            return result;
        }

        public async Task<TEntity> GetAsync(
            Expression<Func<TEntity, bool>> specification,
            Expression<Func<TEntity, object>>[] prefetches = null)
        {
            var model = new QueryCommandModel<TEntity, TKey>(specification);
            model.Prefetches = prefetches;
            var result = await this.GetAsync(model);
            return result;
        }

        public async Task<TEntity> GetByIdAsync(TKey id, Expression<Func<TEntity, object>>[] prefetches = null)
        {
            var model = new EntityKeySingleCommandModel<TEntity, TKey>(id);
            model.Prefetches = prefetches;
            var result = await this.GetAsync(model);
            return result;
        }

        public async Task<IList<TEntity>> ListAsync(
            Expression<Func<TEntity, bool>> specification=null,
            Expression<Func<TEntity, object>>[] prefetches = null)
        {
            var model = new QueryCommandModel<TEntity, TKey>(specification);
            model.Prefetches = prefetches;
            var result = await this.ListAsync(model);
            return result;
        }

        public async Task<IList<TEntity>> ListAsync<TModel>(TModel model)
            where TModel : IQueryCommmandModel<TEntity, TKey>
        {
            var result =
                await
                    this.commandDispatcher
                        .DispatchAsync<IQueryCommmandModel<TEntity, TKey>, TEntity, TKey, IEnumerable<TEntity>>(model);
            return result.ToList();
        }

        public async Task<TKey> SaveAsync(TEntity entity)
        {
            var model = new SaveEntityCommandModel<TEntity, TKey>(entity);
            await
                this.commandDispatcher.DispatchAsync<SaveEntityCommandModel<TEntity, TKey>, TEntity, TKey, TKey>(model);
            await this.unitOfWork.ApplyChangesAsync();
            return entity.Id;
        }
    }
}