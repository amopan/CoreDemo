﻿namespace CoreDemo.Common.Exceptions
{
    using System;

    public interface IExceptionHandler
    {
        Exception HandleException(Exception exception, Guid eventId);
    }
}