﻿namespace CoreDemo.Common.Exceptions
{
    using System;

    public interface IExceptionHandlingPolicy
    {
        Exception HandleError(Exception exception);
    }
}