﻿namespace CoreDemo.Common.Exceptions
{
    using System;
    using System.Collections.Generic;
    using Microsoft.Extensions.DependencyInjection;
    public class ExceptionHandlingPolicyBuilder
    {
        private readonly IServiceProvider serviceProvider;
        public ExceptionHandlingPolicyBuilder(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        private ExceptionHandlingPolicy handlingPolicy;

        public ExceptionHandlingPolicyBuilder Initialize()
        {
            if (this.handlingPolicy != null) throw new InvalidOperationException("Exception handling engine is already initialized");
            this.handlingPolicy = new ExceptionHandlingPolicy();
            return this;

        }

        private void CheckEngineInitialized()
        {
            if (this.handlingPolicy == null)
                throw new InvalidOperationException("Exception handling engine is not initialized. Call 'Initialize' before.");
        }
        internal List<IExceptionHandler> CurrentRules { get; set; }

        internal void ForExceptionType<T>() where T : Exception
        {
            this.CheckEngineInitialized();
            var exType = typeof(T);
            if (!this.handlingPolicy.Rules.ContainsKey(exType))
            {
                var rules = new List<IExceptionHandler>();
                this.handlingPolicy.Rules.Add(exType, rules);
            }
            
            this.CurrentRules = this.handlingPolicy.Rules[exType];
        }
        internal void AddHandler<T>() where T : IExceptionHandler
        {
            var handler = this.serviceProvider.GetService<T>();
            this.CurrentRules.Add(handler);
        }

        public IExceptionHandlingPolicy BuildPolicy()
        {
            this.CheckEngineInitialized();
            return this.handlingPolicy;
        }
    }

}
