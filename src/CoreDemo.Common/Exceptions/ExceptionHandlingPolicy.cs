﻿namespace CoreDemo.Common.Exceptions
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    public class ExceptionHandlingPolicy : IExceptionHandlingPolicy
    {
        public ExceptionHandlingPolicy()
        {
            Rules = new ConcurrentDictionary<Type, List<IExceptionHandler>>();
        }

        internal IDictionary<Type, List<IExceptionHandler>> Rules { get; set; }

        public Exception HandleError(Exception exception)
        {
            Guid eventId = Guid.NewGuid();
            var result = exception;
            var handlers = this.GetHandlingRulesForException(exception);
            if (!handlers.Any()) result = this.HandleDefault(exception,eventId);
            else foreach (var handler in handlers) result = handler.HandleException(result,eventId);
            return result;
        }

        private List<IExceptionHandler> GetHandlingRulesForException(Exception ex)
        {
            var result = new List<IExceptionHandler>();

            var exType = ex.GetType();
            if (this.Rules.ContainsKey(exType)) result = this.Rules[exType];
            else foreach (var rule in this.Rules) if (exType.GetTypeInfo().IsSubclassOf(rule.Key) && rule.Value.Any()) return rule.Value;
            return result;
        }

        protected virtual Exception HandleDefault(Exception ex,Guid eventId)
        {
            return ex;
        }
    }
}