﻿namespace CoreDemo.Common.Exceptions
{
    using System;

    public static class ExceptionHandlingPolicyBuilderExtender
    {
        public static ExceptionHandlingPolicyBuilder ForException<T>(this ExceptionHandlingPolicyBuilder builder)
            where T : Exception
        {
            builder.ForExceptionType<T>();
            return builder;
        }

        public static ExceptionHandlingPolicyBuilder UseHandler<T>(this ExceptionHandlingPolicyBuilder builder)
            where T : IExceptionHandler
        {
            builder.AddHandler<T>();
            return builder;
        }
    }
}