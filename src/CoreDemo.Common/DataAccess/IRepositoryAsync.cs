﻿namespace CoreDemo.Common.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    using CoreDemo.Common.Entities;

    public interface IRepositoryAsync<T, in TKey>
        where T : class, IEntity<TKey> //where TKey : struct
    {
        #region Public Methods and Operators

        void Add(T entity);

        // Task<IQueryable<T>> AsQueryable(IEnumerable<Expression<Func<T, object>>> prefetches = null);

        void Delete(T entity);

        //Task DeleteWithSaveChanges(T entity);

        //Task Detach(T entity);

        //void Insert(T entity);

        //Task<IQueryable<T>> Query();

        IQueryable<T> Query( Expression<Func<T, bool>> predicate = null,IEnumerable<Expression<Func<T, object>>> prefetches = null);

        Task SaveChangesAsync();

        //void SaveChanges();

        //T Single(TKey id, IEnumerable<Expression<Func<T, object>>> prefetches = null);

        //T Single(Expression<Func<T, bool>> predicate, IEnumerable<Expression<Func<T, object>>> prefetches = null);

        Task<T> SingleAsync(TKey id, IEnumerable<Expression<Func<T, object>>> prefetches = null);

        Task<T> SingleAsync(Expression<Func<T, bool>> predicate, IEnumerable<Expression<Func<T, object>>> prefetches = null);

        Task<IEnumerable<T>> ListAsync(Expression<Func<T, bool>> predicate = null, IEnumerable<Expression<Func<T, object>>> prefetches = null);

        void Update(T entity);

        #endregion
    }
}