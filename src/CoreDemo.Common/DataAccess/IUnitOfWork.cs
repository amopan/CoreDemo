﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreDemo.Common.DataAccess
{
    using CoreDemo.Common.Entities;

    public interface IUnitOfWorkAsync :IDisposable 
    {
        //void Register<T, TKey>(IRepositoryAsync<T, TKey> repository) where T : class, IEntity<TKey>;

        Task<int> ApplyChangesAsync();

        Task BeginTransaction();

        Task<int> Commit();

        void Rollback();
    }
}
