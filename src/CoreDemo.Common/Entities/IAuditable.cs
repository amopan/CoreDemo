﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreDemo.Common.Entities
{
    public interface IAuditable
    {
            DateTime CreatedOn { get; set; }

            string CreatedBy { get; set; }

            DateTime UpdatedOn { get; set; }

            string UpdatedBy { get; set; }
    }
}
