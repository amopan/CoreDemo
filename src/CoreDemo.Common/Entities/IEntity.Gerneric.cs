﻿namespace CoreDemo.Common.Entities
{
    public interface IEntity<T> : IEntity

    {
        #region Public Properties

        T Id { get; set; }

        #endregion
    }
}