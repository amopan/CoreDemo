﻿namespace CoreDemo.Common.CQRS
{
    using System.Threading.Tasks;
    using CoreDemo.Common.Entities;

    public static class EntityServiceExt
    {
        public static async Task<int> DeleteByIdAsync<TEntity, TKey>(this IEntityServiceAsync<TEntity, TKey> entityServiceAsync, TKey id)
            where TEntity : class, IEntity<TKey>, new()
        {
            return  await entityServiceAsync.DeleteAsync(new TEntity { Id = id });
        }
    }
}