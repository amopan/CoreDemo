﻿namespace CoreDemo.Common.CQRS
{
    using CoreDemo.Common.Entities;

    public interface ICommandModel<TEntity, TKey> : ICommandModel
        where TEntity : class, IEntity<TKey>
    {
    }
}