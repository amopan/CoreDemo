﻿namespace CoreDemo.Common.CQRS
{
    using System;
    using System.Linq.Expressions;

    using CoreDemo.Common.Entities;

    public class SortParameter<TEntity, TKey>
        where TEntity : class, IEntity<TKey>
    {
        #region Public Properties

        public bool Descending { get; set; }

        public Expression<Func<TEntity, object>> OrderExpression { get; set; }

        #endregion
    }
}