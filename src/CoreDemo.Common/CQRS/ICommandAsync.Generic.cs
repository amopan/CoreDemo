﻿namespace CoreDemo.Common.CQRS
{
    using System.Threading.Tasks;

    public interface ICommandAsync<in TModel, TResult> : ICommandAsync
        where TModel : ICommandModel
    {
        #region Public Methods and Operators

        Task<TResult> ExecuteAsync(TModel model);

        #endregion
    }
}