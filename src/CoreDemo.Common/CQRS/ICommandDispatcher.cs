﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreDemo.Common.CQRS
{
    using CoreDemo.Common.Entities;

    public interface ICommandDispatcher
    {
        #region Public Methods and Operators

        Task<TResult> DispatchAsync<TModel, TEntity, TKey, TResult>(TModel commandModel)
            where TModel : ICommandModel<TEntity, TKey> where TEntity : class, IEntity<TKey>;

        



        #endregion
    }
}
