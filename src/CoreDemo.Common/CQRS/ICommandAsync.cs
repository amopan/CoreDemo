﻿namespace CoreDemo.Common.CQRS
{
    using System.Threading.Tasks;

    public interface ICommandAsync
    {
        #region Public Methods and Operators


        Task<object> ExecuteAsync(ICommandModel model);

        #endregion
    }
}