﻿namespace CoreDemo.Common.CQRS
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    using CoreDemo.Common.Entities;

    public interface IEntityServiceAsync<TEntity, TKey>
        where TEntity : class, IEntity<TKey>
    {
        #region Public Methods and Operators

        //void Delete(TKey id); this method requires new() and can not be assignable to abstract classes

        //void Delete(TEntity entity);
        Task<int> DeleteAsync(TEntity entity);

        Task<TEntity> GetAsync<TModel>(TModel model) where TModel : IQueryCommmandModel<TEntity, TKey>;
        Task<TEntity> GetAsync(Expression<Func<TEntity, bool>> specification, Expression<Func<TEntity, object>>[] prefetches = null);

        Task<TEntity> GetByIdAsync(TKey id, Expression<Func<TEntity, object>>[] prefetches = null);

        Task<IList<TEntity>> ListAsync(Expression<Func<TEntity, bool>> specification=null, Expression<Func<TEntity, object>>[] prefetches = null);

        Task<IList<TEntity>> ListAsync<TModel>(TModel model) where TModel : IQueryCommmandModel<TEntity, TKey>;

        Task<TKey> SaveAsync(TEntity entity);

        #endregion
    }
}