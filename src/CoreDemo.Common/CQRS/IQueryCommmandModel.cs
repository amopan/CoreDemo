﻿namespace CoreDemo.Common.CQRS
{
    using System;
    using System.Linq.Expressions;

    using CoreDemo.Common.Entities;

    public interface IQueryCommmandModel<TEntity, TKey> : ICommandModel<TEntity, TKey>
        where TEntity : class, IEntity<TKey>
    {
        #region Public Methods and Operators

        Expression<Func<TEntity, bool>> Specification();

        #endregion

        #region Public Properties

        Expression<Func<TEntity, object>>[] Prefetches { get; set; }

        int RowsCount { get; set; }

        int Skip { get; set; }

        SortParameter<TEntity, TKey>[] SortParameters { get; set; }

        #endregion
    }
}