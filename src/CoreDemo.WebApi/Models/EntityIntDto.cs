﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreDemo.WebApi.Models
{
    public class EntityIntDto
    {
        #region Public Properties

        public int Id { get; set; }

        #endregion
    }
}
