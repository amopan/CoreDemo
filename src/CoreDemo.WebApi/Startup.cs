namespace CoreDemo.WebApi
{
    using System;
    using System.IdentityModel.Tokens.Jwt;
    using System.Security.Claims;
    using System.Text;
    using System.Threading.Tasks;

    using Autofac;
    using Autofac.Extensions.DependencyInjection;

    using AutoMapper;

    using  CoreDemo.Common.Exceptions;
    using CoreDemo.Persistance;
    using CoreDemo.WebApi.Configuration;
    using CoreDemo.WebApi.Configuration.AuthorizationRequirements;
    using CoreDemo.WebApi.Configuration.ExceptionHandlers;
    using CoreDemo.WebApi.Configuration.Exceptions;

    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Diagnostics;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Http.Features.Authentication;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Routing;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    public class Startup
    {
        //private AutoMapperServiceConfiguration mapperConfiguration;
        public Startup(IHostingEnvironment env)
        {
            var builder =
                new ConfigurationBuilder().SetBasePath(env.ContentRootPath)
                    .AddJsonFile("appsettings.json", true, true)
                    .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true);

            if (env.IsEnvironment("Development")) builder.AddApplicationInsightsSettings(true);

            builder.AddEnvironmentVariables();
            this.Configuration = builder.Build();
        }

        public IContainer ApplicationContainer { get; private set; }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddApplicationInsightsTelemetry(this.Configuration);
            services.AddCors(
                options =>
                {
                    // this defines a CORS policy called "default"
                    options.AddPolicy(
                        "default",
                        policy => { policy.WithOrigins("http://localhost:44888").AllowAnyHeader().AllowAnyMethod(); });
                });
            services.AddMvcCore()
                .AddAuthorization(
                    options =>
                    {
                        options.AddPolicy(
                            "PersonalUserDataPolicy",
                            policy => policy.Requirements.Add(new UserPersonalDataRequirement()));
                    })
                .AddJsonFormatters();
            // Create the container builder.
            var builder = new ContainerBuilder();
            this.RegisterDbContextOptions(builder);
            this.RegisterAutomapper(builder);
            builder.RegisterModule<WebApiModule>();

            builder.Populate(services);

            this.ApplicationContainer = builder.Build();
            // Create the IServiceProvider based on the container.
            return new AutofacServiceProvider(this.ApplicationContainer);
        }

        private void RegisterDbContextOptions(ContainerBuilder builder)
        {
            builder.Register(
                (c, p) =>
                {
                    var connectionString = this.Configuration.GetConnectionString("DefaultConnection");
                    var optionsBuilder = new DbContextOptionsBuilder<MyDbContext>();
                    optionsBuilder.UseSqlServer(connectionString);
                    var opt = optionsBuilder.Options;
                    return opt;
                });
        }

        private void RegisterAutomapper(ContainerBuilder builder)
        {
            //this.mapperConfiguration =new MapperConfiguration(cfg => { cfg.AddProfile(new AutoMapperProfileConfiguration()); });
            //services.AddSingleton(sp => this.mapperConfiguration.CreateMapper());
            builder.Register(
                (c, p) =>
                {
                    var mapperConfiguration =
                        new MapperConfiguration(cfg => { cfg.AddProfile(new AutoMapperProfileConfiguration()); });
                    return mapperConfiguration.CreateMapper();
                }).SingleInstance();
        }

        public void Configure(
            IApplicationBuilder app,
            IHostingEnvironment env,
            ILoggerFactory loggerFactory,
            IApplicationLifetime appLifetime)
        {
            loggerFactory.AddLoggers();

            app.UseApplicationInsights();
            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();
            app.UseCors("default");
            app.UseExceptionHandler(
                errorApp =>
                {
                    errorApp.Run(
                        async context =>
                        {
                            var error = context.Features.Get<IExceptionHandlerFeature>();
                            if (error != null)
                            {
                                var ex = error.Error;
                                var exceptionHandlingPolicy =
                                    context.RequestServices.GetService<IExceptionHandlingPolicy>();
                                var options = context.RequestServices.GetService<IOptions<MvcJsonOptions>>();
                                var handledResult = exceptionHandlingPolicy.HandleError(ex);

                                context.Response.ContentType = "application/json";
                                context.Response.StatusCode = 500;
                                var operationErrorDto = handledResult as HttpOperationException;
                                if (operationErrorDto != null)
                                {
                                    var operationError = operationErrorDto;
                                    context.Response.StatusCode = (int)operationError.Status;
                                }
                                var responseDto = HttpErrorDto.FromException(handledResult);
                                var responseJson = JsonConvert.SerializeObject(responseDto,options.Value.SerializerSettings);
                                await context.Response.WriteAsync(responseJson, Encoding.UTF8);
                            }
                        });
                });
            
            //app.UseMiddleware<KestrelAuthenticationMiddleware>();
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
            app.UseIdentityServerAuthentication(
                new IdentityServerAuthenticationOptions
                {
                    Authority = "http://localhost:4446",
                    ScopeName = "coredemo-api",
                    RequireHttpsMetadata = false
                });

            app.UseMvc();

            appLifetime.ApplicationStopped.Register(() => this.ApplicationContainer.Dispose());
        }

        public class KestrelAuthenticationMiddleware
        {
            private readonly RequestDelegate next;

            public KestrelAuthenticationMiddleware(RequestDelegate next)
            {
                this.next = next;
            }

            public async Task Invoke(HttpContext context)
            {
                var existingPrincipal = context.Features.Get<IHttpAuthenticationFeature>()?.User;
                var handler = new KestrelAuthHandler(context, existingPrincipal);
                this.AttachAuthenticationHandler(handler);
                try
                {
                    await this.next(context);
                }
                finally
                {
                    this.DetachAuthenticationhandler(handler);
                }
            }

            private void AttachAuthenticationHandler(KestrelAuthHandler handler)
            {
                var auth = handler.HttpContext.Features.Get<IHttpAuthenticationFeature>();
                if (auth == null)
                {
                    auth = new HttpAuthenticationFeature();
                    handler.HttpContext.Features.Set(auth);
                }
                handler.PriorHandler = auth.Handler;
                auth.Handler = handler;
            }

            private void DetachAuthenticationhandler(KestrelAuthHandler handler)
            {
                var auth = handler.HttpContext.Features.Get<IHttpAuthenticationFeature>();
                if (auth != null) auth.Handler = handler.PriorHandler;
            }
        }

        internal class KestrelAuthHandler : IAuthenticationHandler
        {
            internal KestrelAuthHandler(HttpContext httpContext, ClaimsPrincipal user)
            {
                this.HttpContext = httpContext;
                this.User = user;
            }

            internal HttpContext HttpContext { get; }

            internal ClaimsPrincipal User { get; }

            internal IAuthenticationHandler PriorHandler { get; set; }

            public Task AuthenticateAsync(AuthenticateContext context)
            {
                if (this.User != null) context.Authenticated(this.User, null, null);
                else context.NotAuthenticated();

                if (this.PriorHandler != null) return this.PriorHandler.AuthenticateAsync(context);

                return Task.FromResult(0);
            }

            public Task ChallengeAsync(ChallengeContext context)
            {
                var handled = false;
                switch (context.Behavior)
                {
                    case ChallengeBehavior.Automatic:
                        // If there is a principal already, invoke the forbidden code path
                        if (this.User == null) goto case ChallengeBehavior.Unauthorized;
                        goto case ChallengeBehavior.Forbidden;
                    case ChallengeBehavior.Unauthorized:
                        this.HttpContext.Response.StatusCode = 401;
                        // We would normally set the www-authenticate header here, but IIS does that for us.
                        break;
                    case ChallengeBehavior.Forbidden:
                        this.HttpContext.Response.StatusCode = 403;
                        handled = true; // No other handlers need to consider this challenge.
                        break;
                }
                context.Accept();

                if (!handled && (this.PriorHandler != null)) return this.PriorHandler.ChallengeAsync(context);

                return Task.FromResult(0);
            }

            public void GetDescriptions(DescribeSchemesContext context)
            {
                this.PriorHandler?.GetDescriptions(context);
            }

            public Task SignInAsync(SignInContext context)
            {
                // Not supported, fall through
                if (this.PriorHandler != null) return this.PriorHandler.SignInAsync(context);

                return Task.FromResult(0);
            }

            public Task SignOutAsync(SignOutContext context)
            {
                // Not supported, fall through
                if (this.PriorHandler != null) return this.PriorHandler.SignOutAsync(context);

                return Task.FromResult(0);
            }
        }
    }

    public static class StartupLoggerExtentions
    {
        public static ILoggerFactory AddLoggers(this ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole();
            loggerFactory.AddDebug();
            return loggerFactory;
        }
    }

    public static class StartupInsightsExtentions
    {
        public static IApplicationBuilder UseApplicationInsights(this IApplicationBuilder app)
        {
            app.UseApplicationInsightsRequestTelemetry();

            app.UseApplicationInsightsExceptionTelemetry();
            return app;
        }
    }

    public static class StartupRoutingExtentions
    {
        public static IRouteBuilder AddRoutes(this IRouteBuilder routeBuilder)
        {
            //routeBuilder.Routes.Clear();
            routeBuilder.MapRoute("default_route", "api/{controller}/{action=Get}/{id?}");
            return routeBuilder;
        }
    }
}