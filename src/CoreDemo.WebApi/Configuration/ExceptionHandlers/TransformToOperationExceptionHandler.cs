﻿namespace CoreDemo.WebApi.Configuration.ExceptionHandlers
{
    using System;
    using System.Diagnostics.Contracts;
    using System.Net;

    using CoreDemo.WebApi.Configuration.Exceptions;

    public class TransformToOperationExceptionHandler<TException> : TransformExceptionHandler<HttpOperationException> where TException : Exception
    {
        protected override HttpOperationException TransformException(Exception exception,Guid eventId)
        {
            var typedEx = exception as TException;
            if (typedEx != null)
            {
                return this.ToOperationException(typedEx,eventId);
            }
            return this.CreateOperationException(exception,eventId);
        }

        protected virtual HttpOperationException ToOperationException(TException exception, Guid eventId)
        {
            return this.CreateOperationException(exception,eventId);
        }
        private HttpOperationException CreateOperationException(Exception exception, Guid eventId)
        {
            Contract.Ensures(exception != null);
            string errorCode = exception.GetType().FullName;
            var result = new HttpOperationException(errorCode, exception.Message);
            result.HandlingInstanceId = eventId;
            return result;
        }
    }
    public class TransformApiExceptionExceptionToOperationExceptionHandler<T> : TransformToOperationExceptionHandler<T>
        where T : ApiException
    {
        protected override HttpOperationException ToOperationException(T exception, Guid eventId)
        {
            var result = base.ToOperationException(exception,eventId);
            result.ErrorCode = exception.ErrorCode;
            return result;
        }
    }

    public class TransformNotFoundExceptionToOperationExceptionHandler :
        TransformApiExceptionExceptionToOperationExceptionHandler<NotFoundException>
    {
        protected override HttpOperationException ToOperationException(NotFoundException exception, Guid eventId)
        {
            var result = base.ToOperationException(exception,eventId);
            result.Status = HttpStatusCode.NotFound;
            return result;
        }
    }
}
