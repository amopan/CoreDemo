﻿namespace CoreDemo.WebApi.Configuration.ExceptionHandlers
{
    using System;

    using CoreDemo.Common.Exceptions;

    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Logging.Internal;

    public class WriteLogHandler : IExceptionHandler
    {
        private readonly ILogger logger;

        protected LogLevel LogLevel { get; set; }

        public WriteLogHandler(ILoggerFactory logger)
        {
            this.logger = logger.CreateLogger("ErrorLogger");
            this.LogLevel = LogLevel.Debug;
        }


        public Exception HandleException(Exception exception, Guid eventGuid)
        {
            var eventId = new EventId(0, eventGuid.ToString());
            this.logger.Log(
                this.LogLevel,
                eventId,
                new FormattedLogValues(exception.Message),
                exception,
                messageFormatter);
            return exception;
        }
        private static readonly Func<object, Exception, string> messageFormatter = MessageFormatter;
        private static string MessageFormatter(object state, Exception error)
        {
            return state.ToString();
        }
    }
    public class LogErrorHandler : WriteLogHandler
    {
        public LogErrorHandler(ILoggerFactory logger)
            : base(logger)
        {
            this.LogLevel = LogLevel.Error;
        }
    }
    public class LogCriticalHandler : WriteLogHandler
    {
        public LogCriticalHandler(ILoggerFactory logger)
            : base(logger)
        {
            this.LogLevel = LogLevel.Critical;
        }
    }

    public class LogWarningHandler : WriteLogHandler
    {
        public LogWarningHandler(ILoggerFactory logger)
            : base(logger)
        {
            this.LogLevel = LogLevel.Warning;
        }
    }
}