﻿using System;


namespace CoreDemo.WebApi.Configuration.ExceptionHandlers
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Net;
    

    public class HttpErrorDto
    {
        public string Message { get; set; }
        public HttpStatusCode Status { get; set; }

        public string ErrorCode { get; set; }

        public Guid HandlingInstanceId { get; set; }

        public IDictionary<string,object> Data { get; set; }

        public HttpErrorDto()
        {
            this.Data = new Dictionary<string,object>();
        }

        public static HttpErrorDto FromOperationException(HttpOperationException exception)
        {
            return new HttpErrorDto
                   {
                       Message = exception.Message,
                       Status = exception.Status,
                       ErrorCode = exception.ErrorCode,
                       HandlingInstanceId = exception.HandlingInstanceId,
                       Data = exception.Data
                   };
        }

        public static HttpErrorDto FromException(Exception exception)
        {
            if (exception is HttpOperationException)
            {
                return FromOperationException((HttpOperationException)exception);
            }

            return new HttpErrorDto
            {
                Message = exception.Message,
                Status = HttpStatusCode.InternalServerError,
                ErrorCode = exception.GetType().FullName,
                HandlingInstanceId = Guid.Empty
            };
        }
    }
}
