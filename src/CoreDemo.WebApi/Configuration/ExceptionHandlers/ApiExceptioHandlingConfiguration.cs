﻿namespace CoreDemo.WebApi.Configuration.ExceptionHandlers
{
    using System;

    using CoreDemo.Common.Exceptions;
    using CoreDemo.WebApi.Configuration.Exceptions;

    public static class ApiExceptioHandlingConfiguration
    {
        public static IExceptionHandlingPolicy BuildApiExceptionHandlingPoilicy(
            this ExceptionHandlingPolicyBuilder builder)
        {
            return
                builder.Initialize()
                    .ForException<ApiException>()
                    .UseHandler<LogErrorHandler>()
                    .UseHandler<TransformToOperationExceptionHandler<ApiException>>()

                    .ForException<NotFoundException>()
                    .UseHandler<LogWarningHandler>()
                    .UseHandler<TransformToOperationExceptionHandler<NotFoundException>>()

                    .ForException<Exception>()
                    .UseHandler<LogErrorHandler>()
                    .UseHandler<TransformToOperationExceptionHandler<Exception>>()

                    .BuildPolicy();
        }
    }
}