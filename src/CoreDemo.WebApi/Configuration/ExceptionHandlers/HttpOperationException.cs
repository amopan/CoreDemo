﻿namespace CoreDemo.WebApi.Configuration.ExceptionHandlers
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.Contracts;
    using System.Net;

    public class HttpOperationException:Exception
    {
        public HttpOperationException(string code, string message,HttpStatusCode status=HttpStatusCode.InternalServerError)
            :base(message)
        {
            this.Status = status;
            Contract.Assert(code != null);
            Contract.Assert(message != null);
            this.ErrorCode = code;
            this.Data = new Dictionary<string, object>();
        }

        public HttpStatusCode Status { get; set; }

        public string ErrorCode { get; set; }

        public Guid HandlingInstanceId { get; set; }

        public new IDictionary<string,object> Data { get;  }
        
    }
}