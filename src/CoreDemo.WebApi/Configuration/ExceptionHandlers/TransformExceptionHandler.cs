﻿


namespace CoreDemo.WebApi.Configuration.ExceptionHandlers
{
    using System;
    using CoreDemo.Common.Exceptions;
    public abstract class TransformExceptionHandler<TExceptionDest> : IExceptionHandler where TExceptionDest : Exception
    {
        public Exception HandleException(Exception exception,Guid eventId)
        {
            return this.TransformException(exception,eventId);
        }

        protected abstract TExceptionDest TransformException(Exception exception,Guid eventId);
    }

}
