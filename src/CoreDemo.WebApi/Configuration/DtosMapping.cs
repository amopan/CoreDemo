﻿namespace CoreDemo.WebApi.Configuration
{
    using AutoMapper;

    using CoreDemo.Data.Users;

    using Models.Users;

    using Microsoft.Extensions.DependencyInjection;

    public class AutoMapperProfileConfiguration : Profile
    {
        public AutoMapperProfileConfiguration()
        {
            this.CreateMap<User, UserDto>();
            this.CreateMap<UserDto, User>();
        }
    }

    
}