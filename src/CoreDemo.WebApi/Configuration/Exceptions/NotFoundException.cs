﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreDemo.WebApi.Configuration.Exceptions
{
    public class NotFoundException:ApiException
    {
        const string NotFoundExceptionErrorCode = "ResourceNotFond";

        public NotFoundException():this(string.Empty,null)
        {
        }

        public NotFoundException(string message)
            : this(message,null)
        {
        }

        public NotFoundException(string message, Exception innerException)
            : base(message, innerException)
        {
            this.ErrorCode = NotFoundExceptionErrorCode;
        }
    }
}
