﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreDemo.WebApi.Configuration.Exceptions
{
    using CoreDemo.Common.Exceptions;
    public class ApiException:ExceptionBase
    {
        const string ApiExceptionErrorCode = "APIException";
        public string ErrorCode { get; protected set; }

        public ApiException():this(string.Empty,null)
        {
        }

        public ApiException(string message)
            : this(message,null)
        {
        }

        public ApiException(string message, Exception innerException)
            : base(message, innerException)
        {
            this.ErrorCode = ApiExceptionErrorCode;
        }
    }
}
