﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreDemo.WebApi.Configuration.AuthorizationHandlers
{
    using System.Security.Claims;

    using CoreDemo.WebApi.Configuration.AuthorizationRequirements;

    using IdentityModel;

    using Microsoft.AspNetCore.Authorization;

    public class UserPersonalDataHandler : AuthorizationHandler<UserPersonalDataRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, UserPersonalDataRequirement requirement)
        {

            if (context.User.HasClaim(c => c.Type == JwtClaimTypes.Subject))
            {
                context.Succeed(requirement);
                return Task.CompletedTask;
            }
            
            return Task.CompletedTask;
        }
    }
}
