﻿namespace CoreDemo.WebApi.Configuration
{
    using System;

    using Autofac;

    using AutoMapper;

    using CoreDemo.Business.Configuration;

    
    using CoreDemo.Common.Exceptions;
    using CoreDemo.Persistance.Configuration;
    using CoreDemo.Services.Configuration;
    using CoreDemo.WebApi.Configuration.AuthorizationHandlers;
    using CoreDemo.WebApi.Configuration.Exceptions;
    using CoreDemo.WebApi.Configuration.ExceptionHandlers;
    using CoreDemo.WebApi.Controllers;

    using Microsoft.AspNetCore.Authorization;

    public class WebApiModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {

            this.RegisterWebApiServices(builder);
            
            builder.RegisterModule<PersistanceModule>();
            builder.RegisterModule<BusinessModule>();
            builder.RegisterModule<ServicesModule>();
            base.Load(builder);
        }

        private void RegisterWebApiServices(ContainerBuilder builder)
        {
            builder.RegisterType<ExceptionHandlingPolicyBuilder>().AsSelf().InstancePerLifetimeScope();
            builder.RegisterType<LogErrorHandler>().AsSelf().InstancePerLifetimeScope();
            builder.RegisterGeneric(typeof(TransformToOperationExceptionHandler<>)).AsSelf().InstancePerLifetimeScope();
            builder.RegisterGeneric(typeof(TransformApiExceptionExceptionToOperationExceptionHandler<>)).As(typeof(TransformToOperationExceptionHandler<>)).InstancePerLifetimeScope();
            builder.RegisterType(typeof(TransformNotFoundExceptionToOperationExceptionHandler)).As(typeof(TransformToOperationExceptionHandler<NotFoundException>)).InstancePerLifetimeScope();


            builder.Register(
                (c, p) =>
                {
                    var mapperConfiguration = new MapperConfiguration(cfg => { cfg.AddProfile(new AutoMapperProfileConfiguration()); });
                    return mapperConfiguration.CreateMapper();
                }).SingleInstance();


            builder.Register(
                (c, p) =>{
                    var exBuilder = c.Resolve<ExceptionHandlingPolicyBuilder>();
                    var result = exBuilder.BuildApiExceptionHandlingPoilicy();
                    return result;
                }).As<IExceptionHandlingPolicy>().InstancePerLifetimeScope();

            builder.RegisterType<WebApiCommonServicesBundle>().AsSelf();
            builder.RegisterType<UserPersonalDataHandler>().As<IAuthorizationHandler>();
        }
    }
}