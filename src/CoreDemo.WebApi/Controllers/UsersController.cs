﻿namespace CoreDemo.WebApi.Controllers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using CoreDemo.Common.CQRS;
    using CoreDemo.Data.Users;
    using CoreDemo.WebApi.Configuration.Exceptions;
    using CoreDemo.WebApi.Models.Users;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [Authorize(Roles="Admin")]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private IEntityServiceAsync<User, int> userService;
        public UsersController(WebApiCommonServicesBundle commonSvcs,IEntityServiceAsync<User,int> userService)
            : base(commonSvcs)
        {
            this.userService = userService;
        }

        
        [HttpGet]
        public async Task<UserDto[]> Get()
        {
            var users= await this.userService.ListAsync();
            var userDtos= this.CommonSvcs.Mapper.Map<UserDto[]>(users.ToArray());
            return userDtos;
        }

        [Authorize(Policy = "PersonalUserDataPolicy")]
        [HttpGet("{id}")]
        public async Task<UserDto> Get(int id)
        {
            var user = await this.userService.GetByIdAsync(id);
            if (user == null)
            {
                throw new NotFoundException($"User with id='{id}' not found.");
            }
            var result = this.CommonSvcs.Mapper.Map<UserDto>(user);
            return result;
        }

        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        [HttpPut]
        [Authorize(Policy = "PersonalUserDataPolicy")]
        public void Put(int id, [FromBody] string value)
        {
        }

        [HttpDelete]
        public void Delete(int id)
        {
        }
    }
}