﻿namespace CoreDemo.WebApi.Controllers
{
    using AutoMapper;

    using Microsoft.AspNetCore.Mvc;

    public class WebApiCommonServicesBundle
    {
        public WebApiCommonServicesBundle(IMapper mapper)
        {
            this.Mapper = mapper;
        }

        public IMapper Mapper { get; }
    }

    public class ControllerBase : Controller
    {
        public ControllerBase(WebApiCommonServicesBundle commonSvcs)
        {
            this.CommonSvcs = commonSvcs;
        }

        protected WebApiCommonServicesBundle CommonSvcs { get; set; }
    }
}