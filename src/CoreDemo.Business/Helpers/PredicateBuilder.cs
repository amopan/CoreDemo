﻿namespace CoreDemo.Business.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;

    public class ExpressionParameterReplacer : ExpressionVisitor
    {
        #region Constructors and Destructors

        public ExpressionParameterReplacer(
            IList<ParameterExpression> fromParameters,
            IList<ParameterExpression> toParameters)
        {
            this.ParameterReplacements = new Dictionary<ParameterExpression, ParameterExpression>();
            for (var i = 0; (i != fromParameters.Count) && (i != toParameters.Count); i++) this.ParameterReplacements.Add(fromParameters[i], toParameters[i]);
        }

        #endregion

        #region Properties

        private IDictionary<ParameterExpression, ParameterExpression> ParameterReplacements { get; }

        #endregion

        #region Methods

        protected override Expression VisitParameter(ParameterExpression node)
        {
            ParameterExpression replacement;
            if (this.ParameterReplacements.TryGetValue(node, out replacement)) node = replacement;
            return base.VisitParameter(node);
        }

        #endregion
    }

    public static class PredicateBuilder
    {
        #region Public Methods and Operators

        public static Expression<Func<T, bool>> And<T>(
            this Expression<Func<T, bool>> expr1,
            Expression<Func<T, bool>> expr2)
        {
            var paramReplacer = new ExpressionParameterReplacer(expr2.Parameters, expr1.Parameters);

            var exprBody2 = paramReplacer.Visit(expr2.Body);
            var exprRes = Expression.Lambda<Func<T, bool>>(Expression.AndAlso(expr1.Body, exprBody2), expr1.Parameters);
            return exprRes;
        }

        public static Expression<Func<T, bool>> False<T>()
        {
            return f => false;
        }

        public static Expression<Func<T, bool>> Or<T>(
            this Expression<Func<T, bool>> expr1,
            Expression<Func<T, bool>> expr2)
        {
            var paramReplacer = new ExpressionParameterReplacer(expr2.Parameters, expr1.Parameters);

            var exprBody2 = paramReplacer.Visit(expr2.Body);
            var exprRes = Expression.Lambda<Func<T, bool>>(Expression.OrElse(expr1.Body, exprBody2), expr1.Parameters);
            return exprRes;
        }

        public static Expression<Func<T, bool>> True<T>()
        {
            return f => true;
        }

        #endregion
    }
}