﻿namespace CoreDemo.Business.Commands
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using CoreDemo.Business.Commands.Base;
    using CoreDemo.Business.Helpers;
    using CoreDemo.Common.CQRS;
    using CoreDemo.Common.DataAccess;
    using CoreDemo.Common.Entities;

    using FluentValidation;

    public class GetListCommand<TModel, TEntity, TKey> : QueryCommandBase<TModel, TEntity, TKey, IEnumerable<TEntity>>
        where TModel : IQueryCommmandModel<TEntity, TKey> where TEntity : class, IEntity<TKey> where TKey : struct
    {
        #region Constructors and Destructors

        public GetListCommand(IValidator<TModel> validator, IRepositoryAsync<TEntity, TKey> repository)
            : base(validator, repository)
        {
        }

        #endregion

        #region Methods

        protected override async Task<IEnumerable<TEntity>> ExecuteInnerAsync(TModel commandModel)
        {
            var predicate = PredicateBuilder.True<TEntity>();
            var spec = commandModel.Specification();
            if (spec != null) predicate = predicate.And(spec);
            var prefetches = commandModel.Prefetches;
            var query = await this.Repository.ListAsync(predicate, prefetches); //todo: check async operation
            //todo: implement paging and sorting
            /*if (commandModel.SortParameters != null && commandModel.SortParameters.Length > 0)
            {
                foreach (var sortParameter in commandModel.SortParameters)
                {
                    if (sortParameter.Descending)
                    {
                        query = query.OrderByDescending(sortParameter.OrderExpression);
                    }
                    else
                    {
                        query = query.OrderBy(sortParameter.OrderExpression);
                    }
                }
                if (commandModel.Skip != default(int))
                {
                    query = query.Skip(commandModel.Skip);
                }
                if (commandModel.RowsCount != default(int))
                {
                    query = query.Take(commandModel.RowsCount);
                }
            }*/
            return query;
        }

        #endregion
    }

    public class GetListCommand<TEntity, TKey> : GetListCommand<IQueryCommmandModel<TEntity, TKey>, TEntity, TKey>
        where TEntity : class, IEntity<TKey> where TKey : struct
    {
        #region Constructors and Destructors

        public GetListCommand(
            IValidator<IQueryCommmandModel<TEntity, TKey>> validator,
            IRepositoryAsync<TEntity, TKey> repository)
            : base(validator, repository)
        {
        }

        #endregion
    }
}