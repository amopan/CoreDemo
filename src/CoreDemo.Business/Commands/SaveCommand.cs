﻿namespace CoreDemo.Business.Commands
{
    using System;
    using System.Threading.Tasks;

    using CoreDemo.Business.CommandModels;
    using CoreDemo.Business.Commands.Base;
    using CoreDemo.Common.DataAccess;
    using CoreDemo.Common.Entities;

    using FluentValidation;

    public class SaveCommand<TEntity, TKey> : SaveCommand<SaveEntityCommandModel<TEntity, TKey>, TEntity, TKey>
        where TEntity : class, IEntity<TKey>
    {
        #region Constructors and Destructors

        public SaveCommand(
            IValidator<SaveEntityCommandModel<TEntity, TKey>> validator,
            IRepositoryAsync<TEntity, TKey> repository)
            : base(validator, repository)
        {
        }

        #endregion
    }

    public class SaveCommand<TModel, TEntity, TKey> : CrudCommandBase<TModel, TEntity, TKey>
        where TModel : SaveEntityCommandModel<TEntity, TKey> where TEntity : class, IEntity<TKey>
    {
        #region Constructors and Destructors

        public SaveCommand(IValidator<TModel> validator, IRepositoryAsync<TEntity, TKey> repository)
            : base(validator, repository)
        {
        }

        #endregion

        #region Methods

        protected override async Task<TKey> ExecuteInnerAsync(TModel commandModel)
        {
            var entity = commandModel.Entity;
            this.UpdateAuditableProperties(entity);
            this.Repository.Add(entity);
            await Task.Yield(); //this.Repository.SaveChangesAsync();
            return entity.Id;
        }

        private void UpdateAuditableProperties(TEntity entity)
        {
            var auditable = entity as IAuditable;
            if (auditable != null)
            {
                //auditable.UpdatedBy = System.Threading.Thread.CurrentPrincipal.Identity.Name;
                auditable.UpdatedOn = DateTime.Now;
                if (entity.Id.Equals(default(TKey))) auditable.CreatedOn = DateTime.Now;
            }
        }

        #endregion
    }
}