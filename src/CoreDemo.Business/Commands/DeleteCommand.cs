﻿namespace CoreDemo.Business.Commands
{
    using System.Threading.Tasks;

    using CoreDemo.Business.CommandModels;
    using CoreDemo.Business.Commands.Base;
    using CoreDemo.Common.DataAccess;
    using CoreDemo.Common.Entities;

    using FluentValidation;

    public class DeleteCommand<TEntity, TKey> : DeleteCommand<DeleteEntityCommandModel<TEntity, TKey>, TEntity, TKey>
        where TEntity : class, IEntity<TKey>
    {
        public DeleteCommand(
            IValidator<DeleteEntityCommandModel<TEntity, TKey>> validator,
            IRepositoryAsync<TEntity, TKey> repository)
            : base(validator, repository)
        {
        }
    }

    public class DeleteCommand<TModel, TEntity, TKey> : CrudCommandBase<TModel, TEntity, TKey>
        where TModel : DeleteEntityCommandModel<TEntity, TKey> where TEntity : class, IEntity<TKey>
    {
        #region Constructors and Destructors

        public DeleteCommand(
            IValidator<DeleteEntityCommandModel<TEntity, TKey>> validator,
            IRepositoryAsync<TEntity, TKey> repository)
            : base(validator, repository)
        {
        }

        #endregion

        #region Methods

        protected override async Task<TKey> ExecuteInnerAsync(TModel commandModel)
        {
            var res = await Task<TKey>.Factory.StartNew(
                () =>
                {
                    var key = commandModel.Entity.Id;
                    this.Repository.Delete(commandModel.Entity);

                    return key;
                });
            return res;
        }

        #endregion
    }
}