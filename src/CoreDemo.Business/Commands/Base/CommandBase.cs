﻿namespace CoreDemo.Business.Commands.Base
{
    using System.Diagnostics.Contracts;
    using System.Linq;
    using System.Threading.Tasks;

    using CoreDemo.Business.Exceptions;
    using CoreDemo.Common.CQRS;

    using FluentValidation;

    public abstract class CommandBase<T, TResult> : ICommandAsync<T, TResult>
        where T : ICommandModel
    {
        #region Constructors and Destructors

        protected CommandBase(IValidator<T> validator)
        {
            this.Validator = validator;
        }

        #endregion

        #region Public Properties

        public IValidator<T> Validator { get; }

        #endregion

        #region Public Methods and Operators

        #endregion

        #region Explicit Interface Methods

        async Task<object> ICommandAsync.ExecuteAsync(ICommandModel model)
        {
            return await this.ExecuteAsync((T)model);
        }

        public async Task<TResult> ExecuteAsync(T model)
        {
            await this.Validate(model);
            var result = await this.ExecuteInnerAsync(model);
            return result;
        }

        #endregion

        #region Methods

        protected abstract Task<TResult> ExecuteInnerAsync(T model);

        protected virtual async Task Validate(T command)
        {
            Contract.Requires(this.Validator != null, "CommandValidator can not be null");
            var result = await this.Validator.ValidateAsync(command);
            if (!result.IsValid)
            {
                var failures =
                    result.Errors.Where(e => e != null)
                        .Select(
                            e =>
                                new ValidationFailureInfo(
                                    e.PropertyName,
                                    e.ErrorMessage,
                                    e.CustomState as string,
                                    e.AttemptedValue,
                                    e.CustomState));
                throw new BusinessValidationException(failures.ToList());
            }
        }

        #endregion
    }
}