﻿namespace CoreDemo.Business.Commands.Base
{
    using CoreDemo.Business.CommandModels;
    using CoreDemo.Common.DataAccess;
    using CoreDemo.Common.Entities;

    using FluentValidation;

    public abstract class CrudCommandBase<TModel, TEntity, TKey> : DomainCommandBase<TModel, TEntity, TKey, TKey>
        where TModel : EntityCommandModel<TEntity, TKey> where TEntity : class, IEntity<TKey>
    {
        public CrudCommandBase(IValidator<TModel> validator, IRepositoryAsync<TEntity, TKey> repository)
            : base(validator, repository)
        {
        }
    }
}