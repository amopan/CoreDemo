﻿namespace CoreDemo.Business.Commands.Base
{
    using CoreDemo.Common.CQRS;
    using CoreDemo.Common.DataAccess;
    using CoreDemo.Common.Entities;

    using FluentValidation;

    public abstract class DomainCommandBase<TModel, TEntity, TKey, TResult> : CommandBase<TModel, TResult>
        where TModel : ICommandModel<TEntity, TKey> where TEntity : class, IEntity<TKey>
    {
        #region Constructors and Destructors

        protected DomainCommandBase(IValidator<TModel> validator, IRepositoryAsync<TEntity, TKey> repository)
            : base(validator)
        {
            this.Repository = repository;
        }

        #endregion

        #region Public Properties

        public IRepositoryAsync<TEntity, TKey> Repository { get; }

        #endregion
    }
}