﻿namespace CoreDemo.Business.Commands.Base
{
    using CoreDemo.Common.CQRS;
    using CoreDemo.Common.DataAccess;
    using CoreDemo.Common.Entities;

    using FluentValidation;

    public abstract class QueryCommandBase<TModel, TEntity, TKey, TResult> :
            DomainCommandBase<TModel, TEntity, TKey, TResult>
        where TModel : IQueryCommmandModel<TEntity, TKey> where TEntity : class, IEntity<TKey>
    {
        protected QueryCommandBase(IValidator<TModel> validator, IRepositoryAsync<TEntity, TKey> repository)
            : base(validator, repository)
        {
        }
    }
}