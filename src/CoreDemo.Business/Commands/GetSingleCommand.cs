﻿namespace CoreDemo.Business.Commands
{
    using System.Threading.Tasks;

    using CoreDemo.Business.Commands.Base;
    using CoreDemo.Business.Helpers;
    using CoreDemo.Common.CQRS;
    using CoreDemo.Common.DataAccess;
    using CoreDemo.Common.Entities;

    using FluentValidation;

    public class GetSingleCommand<TModel, TEntity, TKey> : QueryCommandBase<TModel, TEntity, TKey, TEntity>
        where TModel : IQueryCommmandModel<TEntity, TKey> where TEntity : class, IEntity<TKey>
    {
        #region Constructors and Destructors

        public GetSingleCommand(IValidator<TModel> validator, IRepositoryAsync<TEntity, TKey> repository)
            : base(validator, repository)
        {
        }

        #endregion

        #region Methods

        protected override async Task<TEntity> ExecuteInnerAsync(TModel commandModel)
        {
            //return this.Repository.Query.Single(commandModel.Specification);
            var spec = commandModel.Specification();
            var predicate = PredicateBuilder.True<TEntity>();
            if (spec != null) predicate = predicate.And(spec);

            return await this.Repository.SingleAsync(predicate, commandModel.Prefetches);
        }

        #endregion
    }

    public class GetSingleCommand<TEntity, TKey> : GetSingleCommand<IQueryCommmandModel<TEntity, TKey>, TEntity, TKey>
        where TEntity : class, IEntity<TKey>
    {
        #region Constructors and Destructors

        public GetSingleCommand(
            IValidator<IQueryCommmandModel<TEntity, TKey>> validator,
            IRepositoryAsync<TEntity, TKey> repository)
            : base(validator, repository)
        {
        }

        #endregion
    }
}