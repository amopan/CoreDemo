﻿namespace CoreDemo.Business
{
    using System;
    using System.Threading.Tasks;

    using Autofac;

    using CoreDemo.Business.CommandModels;
    using CoreDemo.Business.Commands.Base;
    using CoreDemo.Common.CQRS;
    using CoreDemo.Common.Entities;

    public class CommandDispatcher : ICommandDispatcher
    {
        //private IServiceProvider serviceProvider;
        private readonly IComponentContext componentContext;

        public CommandDispatcher( /*IServiceProvider serviceProvider*/ IComponentContext componentContext)
        {
            //this.serviceProvider = serviceProvider;
            this.componentContext = componentContext;
        }

        public async Task<TResult> DispatchAsync<TModel, TEntity, TKey, TResult>(TModel commandModel)
            where TModel : ICommandModel<TEntity, TKey> where TEntity : class, IEntity<TKey>
        {
            var command = this.GetCommand<TModel, TEntity, TKey, TResult>();
            var result = await command.ExecuteAsync(commandModel);
            return result;
        }

        

        protected DomainCommandBase<TModel, TEntity, TKey, TResult> GetCommand<TModel, TEntity, TKey, TResult>()
            where TModel : ICommandModel<TEntity, TKey> where TEntity : class, IEntity<TKey>
        {
            return this.componentContext.Resolve<DomainCommandBase<TModel, TEntity, TKey, TResult>>();
        }
    }

    public static class CommandDispatcherExt
    {
        public static async Task<TResult> DispatchQuery<TModel, TEntity, TKey, TResult>(this ICommandDispatcher dispatcher, TModel commandModel)
            where TModel : IQueryCommmandModel<TEntity, TKey> where TEntity : class, IEntity<TKey>
        {
            return await dispatcher.DispatchAsync<TModel, TEntity, TKey, TResult>(commandModel);
        }

        public static  async Task<TKey> DispatchCrud<TModel, TEntity, TKey>(this ICommandDispatcher dispatcher,TModel commandModel)
            where TModel : EntityCommandModel<TEntity, TKey> where TEntity : class, IEntity<TKey>
        {
            return await dispatcher.DispatchAsync<TModel, TEntity, TKey, TKey>(commandModel);
        }
    }
}