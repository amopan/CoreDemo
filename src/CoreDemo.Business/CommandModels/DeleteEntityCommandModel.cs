﻿namespace CoreDemo.Business.CommandModels
{
    using CoreDemo.Common.Entities;

    public class DeleteEntityCommandModel<T, TKey> : EntityCommandModel<T, TKey>
        where T : class, IEntity<TKey>
    {
        public DeleteEntityCommandModel()
        {
        }

        public DeleteEntityCommandModel(T entity)
            : base(entity)
        {
        }
    }
}