﻿namespace CoreDemo.Business.CommandModels
{
    using System;
    using System.Linq.Expressions;

    using CoreDemo.Common.Entities;

    public class EntityKeySingleCommandModel<TEntity, TKey> : QueryCommandModel<TEntity, TKey>
        where TEntity : class, IEntity<TKey>
    {
        #region Public Properties

        public TKey Id { get; set; }

        #endregion

        #region Public Methods and Operators

        public override Expression<Func<TEntity, bool>> Specification()
        {
            var itemParameter = Expression.Parameter(typeof(TEntity), "item");
            return
                Expression.Lambda<Func<TEntity, bool>>(
                    Expression.Equal(Expression.Property(itemParameter, "Id"), Expression.Constant(this.Id)),
                    itemParameter);
        }

        #endregion

        #region Constructors and Destructors

        public EntityKeySingleCommandModel(TKey id)
            : this()
        {
            this.Id = id;
        }

        public EntityKeySingleCommandModel()
            : base(null)
        {
        }

        #endregion
    }
}