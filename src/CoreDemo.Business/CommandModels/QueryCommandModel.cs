﻿namespace CoreDemo.Business.CommandModels
{
    using System;
    using System.Linq.Expressions;

    using CoreDemo.Common.CQRS;
    using CoreDemo.Common.Entities;

    public class QueryCommandModel<TEntity, TKey> : IQueryCommmandModel<TEntity, TKey>
        where TEntity : class, IEntity<TKey>
    {
        #region Properties

        protected Expression<Func<TEntity, bool>> InnerSpecification { get; set; }

        #endregion

        #region Public Methods and Operators

        public virtual Expression<Func<TEntity, bool>> Specification()
        {
            return this.InnerSpecification;
        }

        #endregion

        #region Constructors and Destructors

        public QueryCommandModel()
        {
        }

        public QueryCommandModel(Expression<Func<TEntity, bool>> specification)
        {
            this.InnerSpecification = specification;
        }

        #endregion

        #region Public Properties

        public Expression<Func<TEntity, object>>[] Prefetches { get; set; }

        public int RowsCount { get; set; }

        public int Skip { get; set; }

        public SortParameter<TEntity, TKey>[] SortParameters { get; set; }

        #endregion
    }
}