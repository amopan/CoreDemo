﻿namespace CoreDemo.Business.CommandModels
{
    using CoreDemo.Common.Entities;

    public class SaveEntityCommandModel<T, TKey> : EntityCommandModel<T, TKey>
        where T : class, IEntity<TKey>
    {
        public SaveEntityCommandModel()
        {
        }

        public SaveEntityCommandModel(T entity)
            : base(entity)
        {
        }
    }
}