﻿namespace CoreDemo.Business.CommandModels
{
    using CoreDemo.Common.CQRS;
    using CoreDemo.Common.Entities;

    public class EntityCommandModel<T, TKey> : ICommandModel<T, TKey>
        where T : class, IEntity<TKey>
    {
        public EntityCommandModel()
        {
        }

        public EntityCommandModel(T entity)
        {
            this.Entity = entity;
        }

        #region Public Properties

        public T Entity { get; set; }

        #endregion
    }
}