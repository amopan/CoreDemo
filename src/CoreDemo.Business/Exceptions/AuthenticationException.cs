﻿namespace CoreDemo.Business.Exceptions
{
    using System;

    public class AuthenticationException : BusinessException
    {
        #region Constants

        private const string AuthenticationExceptionErrorCode = "AuthenticationException";

        #endregion

        #region Constructors and Destructors

        public AuthenticationException()
            : this(string.Empty, null)
        {
        }

        public AuthenticationException(string message)
            : this(message, null)
        {
        }

        public AuthenticationException(string message, Exception innerException)
            : base(message, innerException)
        {
            this.ErrorCode = AuthenticationExceptionErrorCode;
        }

        #endregion
    }
}