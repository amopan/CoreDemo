﻿namespace CoreDemo.Business.Exceptions
{
    using System;

    using CoreDemo.Common.Exceptions;

    public class BusinessException : ExceptionBase
    {
        private const string DefaultBusinessExceptionErrorCode = "BusinessException";

        public string ErrorCode { get; protected set; }

        #region Constructors and Destructors

        public BusinessException()
            : this(string.Empty, null)
        {
        }

        public BusinessException(string message)
            : this(message, null)
        {
        }

        public BusinessException(string message, Exception innerException)
            : base(message, innerException)
        {
            this.ErrorCode = DefaultBusinessExceptionErrorCode;
        }

        #endregion
    }
}