﻿namespace CoreDemo.Business.CommandValidators
{
    using CoreDemo.Common.CQRS;

    using FluentValidation;

    internal class DefaultValidator<T> : AbstractValidator<T>
        where T : ICommandModel
    {
    }
}