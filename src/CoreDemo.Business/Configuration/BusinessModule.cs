﻿namespace CoreDemo.Business.Configuration
{
    using Autofac;

    using CoreDemo.Business.Commands;
    using CoreDemo.Business.Commands.Base;
    using CoreDemo.Business.CommandValidators;
    using CoreDemo.Common.CQRS;

    using FluentValidation;
    public class BusinessModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            this.ValidatorsBinding(builder);
            this.CommadsBinding(builder);
            this.ServicesBinding(builder);
            base.Load(builder);
        }

        private void ValidatorsBinding(ContainerBuilder builder)
        {
            //ValidatorOptions.ResourceProviderType = typeof(FluentValidationResources);
            builder.RegisterGeneric(typeof(DefaultValidator<>)).As(typeof(IValidator<>));
        }

        private void CommadsBinding(ContainerBuilder builder)
        {
            builder.RegisterGeneric(typeof(SaveCommand<,>)).AsSelf();
            builder.RegisterGeneric(typeof(SaveCommand<,>)).As(typeof(SaveCommand<,,>));
            builder.RegisterGeneric(typeof(SaveCommand<,>)).As(typeof(CrudCommandBase<,,>));
            builder.RegisterGeneric(typeof(SaveCommand<,>)).As(typeof(DomainCommandBase<, , , >));

            builder.RegisterGeneric(typeof(DeleteCommand<,>)).AsSelf();
            builder.RegisterGeneric(typeof(DeleteCommand<,>)).As(typeof(DeleteCommand<,,>));
            builder.RegisterGeneric(typeof(DeleteCommand<,>)).As(typeof(CrudCommandBase<,,>));
            builder.RegisterGeneric(typeof(DeleteCommand<,>)).As(typeof(DomainCommandBase<,,,>));

            builder.RegisterGeneric(typeof(GetSingleCommand<,>)).AsSelf();
            builder.RegisterGeneric(typeof(GetSingleCommand<,>)).As(typeof(QueryCommandBase<,,,>));
            builder.RegisterGeneric(typeof(GetSingleCommand<,>)).As(typeof(GetSingleCommand<,,>));
            builder.RegisterGeneric(typeof(GetSingleCommand<,>)).As(typeof(DomainCommandBase<,,,>));

            builder.RegisterGeneric(typeof(GetListCommand<,>)).AsSelf();
            builder.RegisterGeneric(typeof(GetListCommand<,>)).As(typeof(GetListCommand<,,>));
            builder.RegisterGeneric(typeof(GetListCommand<,>)).As(typeof(QueryCommandBase<,,,>));
            builder.RegisterGeneric(typeof(GetListCommand<,>)).As(typeof(DomainCommandBase<,,,>));

        }

        private void ServicesBinding(ContainerBuilder builder)
        {
            builder.RegisterType<CommandDispatcher>().As<ICommandDispatcher>();
        }
    }
}