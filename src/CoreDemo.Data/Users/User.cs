﻿namespace CoreDemo.Data.Users
{
    using CoreDemo.Data.Base;

    public class User:EntityBase<int>
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }


    }
}
