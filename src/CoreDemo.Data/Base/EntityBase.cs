﻿namespace CoreDemo.Data.Base
{
    using CoreDemo.Common.Entities;

    public abstract class EntityBase<T> : IEntity<T>
    {
        #region Public Properties

        public T Id { get; set; }

        #endregion
    }
}