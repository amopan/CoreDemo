﻿namespace CoreDemo.Data.Base
{
    using System;

    using CoreDemo.Common.Entities;

    public abstract class AuditableEntityBase<TKey> : EntityBase<TKey>, IAuditable
    {
        public DateTime CreatedOn { get; set; }

        public string CreatedBy { get; set; }

        public DateTime UpdatedOn { get; set; }

        public string UpdatedBy { get; set; }
    }
}