﻿namespace CoreDemo.Tests
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    using Autofac;
    using Autofac.Core.Lifetime;

    using CoreDemo.Tests.Configuration;

    using NUnit.Framework;

    public class TestBase
    {
        public IContainer Context { get; set; }

        protected ILifetimeScope ResolutionScope
        {
            get
            {
                return (ILifetimeScope)TestContext.CurrentContext.Test.Properties.Get("ResolutionScope");
            }
            set
            {
                TestContext.CurrentContext.Test.Properties.Set("ResolutionScope", value);
            }
        }

        [OneTimeTearDown]
        public void Destroy()
        {
            this.Context.Dispose();
        }

        [OneTimeSetUp]
        public virtual void Init()
        {
            
            var builder = new ContainerBuilder();
            this.BuildDiModules(builder);
            this.Context = builder.Build();
        }

        protected virtual void BuildDiModules(ContainerBuilder builder)
        {
            builder.RegisterModule<TestModule>();
        }

        [SetUp]
        public virtual void SetUp()
        {
            this.ResolutionScope = this.Context.BeginLifetimeScope(MatchingScopeLifetimeTags.RequestLifetimeScopeTag);
            try
            {
                this.SetUpAsync(ResolutionScope).Wait();
            }
            catch (AggregateException e)
            {
                throw e.InnerExceptions.First();
            }
        }

        protected virtual async Task SetUpAsync(IComponentContext componentContext)
        {
            var task = Task.Factory.StartNew(() => { });
            await task;
        }

        [TearDown]
        public virtual void TearDown()
        {
            this.ResolutionScope.Dispose();
            //this.ResolutionScope = null;
        }
    }
}