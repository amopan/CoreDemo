﻿namespace CoreDemo.Tests.Business.EntityServices
{
    using System.Linq;
    using System.Threading.Tasks;

    using Autofac;

    using CoreDemo.Common.CQRS;
    using CoreDemo.Common.DataAccess;
    using CoreDemo.Data.Users;

    using NUnit.Framework;

    [TestFixture]
    public class UserEntityServiceTests : BusinessTestBase
    {
        private async Task SetupContext(IComponentContext componentContext)
        {
            var userRepository = componentContext.Resolve<IRepositoryAsync<User, int>>();
            var user1 = new User { FirstName = "TestFirst1", LastName = "TestLast1", Email = "TestEmail1@mail.com" };
            var user2 = new User { FirstName = "TestFirst2", LastName = "TestLast2", Email = "TestEmail2@mail.com" };
            var user3 = new User { FirstName = "First3", LastName = "Last3", Email = "Email3@mail.com" };
            userRepository.Add(user1);
            userRepository.Add(user2);
            userRepository.Add(user3);
            await userRepository.SaveChangesAsync();
        }

        protected override async Task SetUpAsync(IComponentContext componentContext)
        {
            await base.SetUpAsync(componentContext);
            await this.SetupContext(componentContext);
        }

        [Test]
        public async Task AddUserTest()
        {
            var scope = this.ResolutionScope;
            var userSvc = scope.Resolve<IEntityServiceAsync<User, int>>();

            var newUser = new User { FirstName = "first", LastName = "last", Email = "email@mail.com" };
            var result = await userSvc.SaveAsync(newUser);
            Assert.That(newUser.Id, Is.GreaterThan(0));
            Assert.That(result, Is.EqualTo(newUser.Id));

            var savedUser = await userSvc.GetByIdAsync(newUser.Id);
            Assert.That(savedUser, Is.Not.Null);
        }

        [Test]
        public async Task DeleteUserTest()
        {
            var scope = this.ResolutionScope;
            var userSvc = scope.Resolve<IEntityServiceAsync<User, int>>();

            var myUser = await userSvc.GetAsync(u => u.Email == "TestEmail1@mail.com");

            await userSvc.DeleteByIdAsync(myUser.Id);
            var resultUser = await userSvc.GetByIdAsync(myUser.Id);

            Assert.That(resultUser, Is.Null);
        }

        [Test]
        public async Task GetUserByIdTest()
        {
            var scope = this.ResolutionScope;
            var userSvc = scope.Resolve<IEntityServiceAsync<User, int>>();

            var myUser = await userSvc.GetAsync(u => u.Email == "TestEmail1@mail.com");

            var resultUser = await userSvc.GetByIdAsync(myUser.Id);
            Assert.That(resultUser, Is.Not.Null);
            Assert.That(resultUser.Id, Is.EqualTo(myUser.Id));
        }

        [Test]
        public async Task GetUserTest()
        {
            var scope = this.ResolutionScope;
            var userSvc = scope.Resolve<IEntityServiceAsync<User, int>>();

            var resultUser = await userSvc.GetAsync(u => u.Email == "TestEmail1@mail.com");
            Assert.That(resultUser, Is.Not.Null);
            Assert.That(resultUser.Email, Is.EqualTo("TestEmail1@mail.com"));
        }

        [Test]
        public async Task ListOfUsersTest()
        {
            var scope = this.ResolutionScope;
            var userSvc = scope.Resolve<IEntityServiceAsync<User, int>>();

            var resultUsers = await userSvc.ListAsync(u => u.FirstName.StartsWith("Test"));
            Assert.That(resultUsers, Is.Not.Empty);
            Assert.That(resultUsers.Any(u => !u.FirstName.StartsWith("Test")),Is.False);
        }

        [Test]
        public async Task UpdateUserTest()
        {
            var scope = this.ResolutionScope;
            var userSvc = scope.Resolve<IEntityServiceAsync<User, int>>();

            var myUser = await userSvc.GetAsync(u => u.Email == "TestEmail1@mail.com");
            myUser.FirstName = "new_First_Name";
            myUser.Email = "new@mail.com";
            await userSvc.SaveAsync(myUser);
            var savedUser = await userSvc.GetByIdAsync(myUser.Id);
            
            Assert.That(savedUser.FirstName,Is.EqualTo(myUser.FirstName));
            Assert.That(savedUser.Email, Is.EqualTo(myUser.Email));
        }
    }
}