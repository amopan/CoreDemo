﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreDemo.Tests.Business
{
    using Autofac;

    using CoreDemo.Business.Configuration;
    using CoreDemo.Services.Configuration;

    public class BusinessTestBase:TestWithTransactionBase
    {
        protected override void BuildDiModules(ContainerBuilder builder)
        {
            base.BuildDiModules(builder);
            builder.RegisterModule<BusinessModule>();
            builder.RegisterModule<ServicesModule>();
        }
    }
}
