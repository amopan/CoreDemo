﻿namespace CoreDemo.Tests.Business.Commands
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using CoreDemo.Common.CQRS;

    using NUnit.Framework;
    using Autofac;
    using Autofac.Core.Lifetime;

    using CoreDemo.Business;
    using CoreDemo.Business.CommandModels;
    using CoreDemo.Business.Commands;
    using CoreDemo.Business.Commands.Base;
    using CoreDemo.Common.DataAccess;
    using CoreDemo.Data.Users;

    [TestFixture]
    public class CommandsTests : BusinessTestBase
    {
        public async Task SetupContext(IComponentContext componentContext)
        {
                var userRepository = componentContext.Resolve<IRepositoryAsync<User, int>>();
                var user1 = new User { FirstName = "TestFirst1", LastName = "TestLast1", Email = "TestEmail1@mail.com" };
                var user2 = new User { FirstName = "TestFirst2", LastName = "TestLast2", Email = "TestEmail2@mail.com" };
                userRepository.Add(user1);
                userRepository.Add(user2);
                await userRepository.SaveChangesAsync();
        }

        protected override async Task SetUpAsync(IComponentContext componentContext)
        {
            await base.SetUpAsync(componentContext);
            await this.SetupContext(componentContext);
        }

        [Test]
        public void QueryCommandResolutionTest()
        {

            var cmd0 = this.ResolutionScope.Resolve<DomainCommandBase<IQueryCommmandModel<User, int>, User, int, IEnumerable<User>>>();
            Assert.That(cmd0, Is.Not.Null);
            Assert.That(cmd0, Is.TypeOf(typeof(GetListCommand<User, int>)));

            var cmd1 = this.ResolutionScope.Resolve<QueryCommandBase<IQueryCommmandModel<User, int>, User, int, User>>();
            Assert.That(cmd1,Is.Not.Null);
            Assert.That(cmd1,Is.TypeOf(typeof(GetSingleCommand<User,int>)));

            var cmd2 = this.ResolutionScope.Resolve<GetSingleCommand<IQueryCommmandModel<User,int>,User,int>>();
            Assert.That(cmd2, Is.Not.Null);
            Assert.That(cmd2, Is.TypeOf(typeof(GetSingleCommand<User, int>)));

            var cmd3 = this.ResolutionScope.Resolve<GetSingleCommand<User, int>>();
            Assert.That(cmd3, Is.Not.Null);
            Assert.That(cmd3, Is.TypeOf(typeof(GetSingleCommand<User, int>)));

            var cmd4 = this.ResolutionScope.Resolve<QueryCommandBase<IQueryCommmandModel<User, int>, User, int, IEnumerable<User>>>();
            Assert.That(cmd4, Is.Not.Null);
            Assert.That(cmd4, Is.TypeOf(typeof(GetListCommand<User, int>)));


        }

        [Test]
        public void CrudCommandResolutionTest()
        {
            var cmd0 = this.ResolutionScope.Resolve<DomainCommandBase<SaveEntityCommandModel<User, int>, User, int,int>>();
            Assert.That(cmd0, Is.Not.Null);
            Assert.That(cmd0, Is.TypeOf(typeof(SaveCommand<User, int>)));


            var cmd1 = this.ResolutionScope.Resolve<CrudCommandBase<SaveEntityCommandModel<User,int>,User,int>>();
            Assert.That(cmd1, Is.Not.Null);
            Assert.That(cmd1, Is.TypeOf(typeof(SaveCommand<User, int>)));

            var cmd2= this.ResolutionScope.Resolve<CrudCommandBase<DeleteEntityCommandModel<User, int>, User, int>>();
            Assert.That(cmd2, Is.Not.Null);
            Assert.That(cmd2, Is.TypeOf(typeof(DeleteCommand<User, int>)));

            var cmd3 = this.ResolutionScope.Resolve<DeleteCommand<User, int>>();
            Assert.That(cmd3, Is.Not.Null);
            Assert.That(cmd3, Is.TypeOf(typeof(DeleteCommand<User, int>)));

        }

        [Test]
        public void  CommandDispatcherResolutionTest()
        {

            var dispatcher = this.ResolutionScope.Resolve<ICommandDispatcher>();
            Assert.That(dispatcher, Is.Not.Null);
            Assert.That(dispatcher, Is.TypeOf<CommandDispatcher>());

        }
        
        [Test]
        public async Task DispatchSaveCommandTest()
        {

            var uow = this.ResolutionScope.Resolve<IUnitOfWorkAsync>();
            var dispatcher = this.ResolutionScope.Resolve<ICommandDispatcher>();

            var newUser = new User {Email = "email",FirstName = "First Name" ,LastName = "Last Name"};
            SaveEntityCommandModel<User,int> model = new SaveEntityCommandModel<User,int>(newUser);
            await dispatcher.DispatchCrud<SaveEntityCommandModel<User, int>, User, int>(model);
            await uow.ApplyChangesAsync();
            Assert.That(newUser.Id,Is.GreaterThan(0));
            

        }

        [Test]
        public async Task DispatchGetCommandTest()
        {
            var dispatcher = this.ResolutionScope.Resolve<ICommandDispatcher>();


            var model = new QueryCommandModel<User, int>(u => u.Email == "TestEmail1@mail.com");
            var result = await dispatcher.DispatchQuery<IQueryCommmandModel<User, int>, User, int, User>(model);
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Id>0);

        }

        [Test]
        public async Task DispatchListCommandTest()
        {
            var dispatcher = this.ResolutionScope.Resolve<ICommandDispatcher>();

           
            var model = new QueryCommandModel<User,int>(u=>u.Id>0);
            var result = await dispatcher.DispatchQuery<IQueryCommmandModel<User, int>, User,int, IEnumerable<User>>(model);
            var resList = result.ToList();
            Assert.That(result,Is.Not.Null);
            Assert.That(resList,Is.Not.Empty);
            Assert.That(resList.Any(x=>x.Id<=0), Is.False);
        }

        [Test]
        public async Task DispatchDeleteCommandTest()
        {
            var scope = this.ResolutionScope;

            var dispatcher = scope.Resolve<ICommandDispatcher>();
            var model = new QueryCommandModel<User, int>(u => u.Email == "TestEmail1@mail.com");
            var user = await dispatcher.DispatchQuery<IQueryCommmandModel<User, int>, User, int, User>(model);
            var deleteModel = new DeleteEntityCommandModel<User,int>(user);
            var uow = scope.Resolve<IUnitOfWorkAsync>();
            await dispatcher.DispatchCrud<DeleteEntityCommandModel<User, int>, User, int>(deleteModel);
            await uow.ApplyChangesAsync();
            var getModel = new QueryCommandModel<User, int>(u => u.Email == "TestEmail1@mail.com");
            var existingUser = await dispatcher.DispatchQuery<IQueryCommmandModel<User, int>, User, int, User>(model);
            Assert.That(existingUser,Is.Null);
        }
        
    }
}