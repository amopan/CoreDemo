﻿namespace CoreDemo.Tests.Persistance
{
    using System.Threading.Tasks;

    using Autofac;
    using Autofac.Core.Lifetime;

    using CoreDemo.Common.DataAccess;
    using CoreDemo.Data.Users;
    using CoreDemo.Persistance.Configuration;

    using Microsoft.EntityFrameworkCore;

    using NUnit.Framework;

    [TestFixture]
    public class PresistanceTests : TestWithTransactionBase
    {

        [Test]

        public void DbContextResolutionTest()
        {
            
                var context = ResolutionScope.Resolve<DbContext>();
                Assert.That(context, Is.Not.Null);
            
            
        }

        [Test]
        public void RepositoryServiceResolutionTest()
        {
                var userRepository = ResolutionScope.Resolve<IRepositoryAsync<User, int>>();
                Assert.That(userRepository, Is.Not.Null);
        }

        [Test]
        public async Task  AddUserTest()
        {
                var uow = ResolutionScope.Resolve<IUnitOfWorkAsync>();
                var userRepository = ResolutionScope.Resolve<IRepositoryAsync<User, int>>();
                var newUser = new User { Email = "email", FirstName = "firstName", LastName = "lastName" };
                userRepository.Add(newUser);
                var i = await uow.ApplyChangesAsync();
                Assert.That(i, Is.GreaterThanOrEqualTo(1));
                Assert.That(newUser.Id, Is.GreaterThan(0));
                var users = await userRepository.ListAsync(u => u.Email == "email");
                Assert.That(users, Is.Not.Empty);

        }
    }
}