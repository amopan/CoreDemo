﻿namespace CoreDemo.Tests.Configuration
{
    using System.IO;

    using Autofac;

    using CoreDemo.Persistance;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;

    public class TestModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var config =
                new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("config.json")
                    .Build();

            builder.Register(
                (c, p) =>
                {
                    var connectionString = config.GetConnectionString("DefaultConnection");
                    var optionsBuilder = new DbContextOptionsBuilder<MyDbContext>();
                    optionsBuilder.UseSqlServer(connectionString);
                    var opt = optionsBuilder.Options;
                    return opt;
                });
            
            base.Load(builder);
        }
    }
}