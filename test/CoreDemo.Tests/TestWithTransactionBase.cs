﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreDemo.Tests
{
    using Autofac;
    using CoreDemo.Common.DataAccess;
    using CoreDemo.Persistance.Configuration;

    public class TestWithTransactionBase : TestBase
    {
        protected override void BuildDiModules(ContainerBuilder builder)
        {
            base.BuildDiModules(builder);
            builder.RegisterModule<PersistanceModule>();

        }
        #region Fields

        private IUnitOfWorkAsync unitOfWork;

        #endregion

        #region Public Methods and Operators

        protected override async Task SetUpAsync(IComponentContext componentContext)
        {
            this.unitOfWork = componentContext.Resolve<IUnitOfWorkAsync>();
            await this.unitOfWork.BeginTransaction();
        }

        public override void TearDown()
        {
            this.unitOfWork.Rollback();
            base.TearDown();
        }

        #endregion
    }
}
